<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class prod_vendidos extends Model
{
	
    protected $fillable = [
        'id',
        'cantidad',
        'producto',
        'total',
        'id_venta',
        'created_at',
        'updated_at'
    ];
}
