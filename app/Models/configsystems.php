<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class configsystems extends Model
{
    protected $fillable = [
    	'nombre_negocio',
        'descripcion',
        'ubicacion',
        'logotipo',
        'nombre_responsable',
        'apellidos',
       	'telefono',
       	'email',
        'precios_producto',
        'created_at',
        'updated_at'

        
    ];
}
