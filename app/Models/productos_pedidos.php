<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class productos_pedidos extends Model
{
    
    protected $fillable = [
        'id',
        'id_pedido',
        'id_producto',
        'cantidad',
        'unidad',
        'precio',
        'importe',
        'created_at',
        'updated_at',
    ];
}
