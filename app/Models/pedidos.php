<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class pedidos extends Model
{
   
    protected $fillable = [
        'id',
        'id_cliente',
        'fecha_pedido',
        'fecha_enviado',
        'fecha_recibido',
        'gps',
        'cp',
        'localidad',
        'colonia',
        'calle',
        'numero_exterior',
        'numero_interior',
        'descripcion_lugar_envio',
        'ubicacion_actual',
        'importe',
        'costo_envio',
        'subtotal',
        'descuento',
        'total',
        'status',
        'created_at',
        'updated_at'
    ];

}
