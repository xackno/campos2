<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class rutas extends Model
{
     protected $fillable = [
        'id',
        'nombre_ruta',
        'longitud_ruta',
        'tiempo_recorrido_total',
        'origen',
        'destino',
        'id_secciones',
        'nombre_secciones',
        'tiempo_secciones',
        'latitud_seccion',
        'created_at',
        'updated_at'
    ];
}
