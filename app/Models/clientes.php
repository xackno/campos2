<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class clientes extends Model
{

    protected $fillable = [
        'nombre',
        'apellidos',
        'municipio',
        'localidad',
        'calle',
        'telefono',
        'correo',
        'usuarioApp',
        'password',
        'status',
        'foto',
        'nivel',
        'calificacion',
        'monedero',
        'identificacion',
        'id_ruta'
    ];
    public function scopeId($query, $id){
        if($id){
            return $query->orWhere('id','=', "$id");
        }
    }//fin function
    public function scopeNombre($query, $nombre){
        if($nombre){
            return $query->orWhere('nombre','LIKE', "%$nombre%");
        }
    }//fin function
     public function scopeApellidos($query, $Apellidos){
        if($Apellidos){
            return $query->orWhere('apellidos','LIKE', "%$Apellidos%");
        }
    }//fin function
     public function scopeNTelefono($query, $Telefono){
        if($Telefono){
            return $query->orWhere('telefono','LIKE', "%$Telefono%");
        }
    }//fin function
     public function scopeIdentificacion($query, $identificacion){
        if($identificacion){
            return $query->orWhere('identificacion','LIKE', "%$identificacion%");
        }
    }//fin function
}
