<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class categorias extends Model
{

    protected $fillable = [
        'id',
        'nombre',
        'descripcion',
        'created_at',
        'updated_at'
    ];
}
