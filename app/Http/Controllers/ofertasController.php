<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ofertas;
class ofertasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ofertas=ofertas::paginate(20);
         return view('forms.ofertas',compact('ofertas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createOferta( Request $request)
    {
        if ($request['id_producto'] == !null && $request['descripcion_producto'] ==!null && $request['foto']==!null) {
            $this->validate($request,[ 'id_producto'=>'required', 'descripcion_producto'=>'required','foto'=>'required']);
            ofertas::create($request->all());
            return back()->with('success','Se agregó a ofertas.');
        }else{
            return back()->with('error','Error al agregar.');
        }
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $producto=ofertas::find($id);
        $producto->delete();
        return back()->with('success','Eliminado correctamente');
    }
}
