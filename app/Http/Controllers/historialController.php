<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ventas;
use App\Models\prod_vendidos;
use Illuminate\Support\Facades\DB;

class historialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ventas=ventas::select('ventas.id','user','id_turno','efectivo','total_venta','tipo','cliente','nombre','domicilio','ventas.created_at')
        ->join('users', 'ventas.usuario', '=', 'users.id')
        ->get();

        return view('forms.historial',compact('ventas'));
    }
    public function list_ajax(Request $request){
        
        $id_venta=$request->input('id_venta');

        $prod_vendidos=prod_vendidos::select('prod_vendidos.id','cantidad','productos.unidad','descripcion_articulo','total','id_venta','fotos')
        ->join('productos', 'prod_vendidos.producto', '=', 'productos.id')
        ->where('id_venta','=','$id_venta')
        ->get();

    // $prod_vendidos=DB::select('SELECT prod_vendidos.id,prod_vendidos.cantidad,productos.unidad,descripcion_articulo,total,id_venta,fotos 
    //      FROM prod_vendidos INNER JOIN productos ON prod_vendidos.producto = productos.id WHERE id_venta=?',[$id_venta]);

        return json_encode($prod_vendidos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
