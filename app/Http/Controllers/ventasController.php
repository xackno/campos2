<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ventas;
use App\Models\prod_vendidos;
use App\Models\productos;
use App\Models\clientes;
use App\Models\configsystems;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
class ventasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('forms.ventas');
    }
    public function paraPedidos()
    {
        $msj="F8";
        $clientes=clientes::all();
        return view('forms.ventas',compact('msj','clientes'));
    }


    public function ventas(Request $data){
        date_default_timezone_set('america/mexico_city');
        $objeto=$data->get("objeto");
        $id=$objeto["id"];
        $cantidad=$objeto["cantidad"];
        $unidad=$objeto["unidad"];
        $descripcion=$objeto["descripcion"];
        $precio=$objeto["precio"];
        $subtotal=$objeto["subtotal"];

        $importe=$data->get("importe");
        $cajero=Auth::user()->name;
        $turno=$data->get("turno");

        $con_ticket=$data->get("con_ticket");
        $status="";
        try {
            $venta=ventas::create([
                "usuario"=>$cajero,
                "id_turno"=>$turno,
                "caja"=>1,
                "efectivo"=>$importe,
                "total_venta"=>array_sum($subtotal),
                "cantidad_pro"=>implode(",",$cantidad),
                "id_productos"=>implode(",",$id),
                "precio_vendido"=>implode(",",$precio)
            ]);
            $status="success";
        } catch (Exception $e) {
            $status="fail";
        }

        if ($con_ticket=="true") {
            $cambio=$importe - array_sum($subtotal);

            $contents = \Storage::disk("files")->get('imprimir.json');
            $proceso = json_decode($contents, true);
            $proceso=$proceso["proceso"]+1;
            $ticket_json=array("proceso"=>$proceso,"tipo"=>"venta",'cajero'=>$cajero,"fecha"=>date("d-m-Y - h:i:s"),'venta'=>$venta->id,"total"=>array_sum($subtotal),"efectivo"=>$importe,"cambio"=>$cambio,
            'id_art'=>$id,'unidad'=>$unidad,'cantidad'=>$cantidad,'descripcion'=>$descripcion,'precio'=>$precio,'importe'=>$subtotal,"cliente"=>"venta al público");
            $json_venta=json_encode($ticket_json);
            \Storage::disk('files')->put("imprimir.json",$json_venta);
        }

        $id_venta=$venta->id;

    return json_encode(compact('id_venta','status'));

    }

}
