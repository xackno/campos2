<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\clientes;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\Models\rutas;
class clientesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rutas=rutas::paginate(20);
        $clientes=clientes::select('clientes.id','nombre','apellidos','municipio','localidad','calle','telefono','correo','status','foto','nivel','calificacion','monedero','identificacion','id_ruta','nombre_ruta')
        ->join('rutas', 'clientes.id_ruta', '=', 'rutas.id')
        ->paginate(20);
        return view('forms.clientes',compact('clientes','rutas'));
    }
    public function rutas(){
        return view('forms.rutas');
    }
    public function see_ruta(Request $request){
        
        $id=$request->input('id');
        $ruta=rutas::find($id);
        return view('forms.see_ruta',compact('ruta'));
    }
     public function edit_ruta(Request $request){
        
        $id=$request->input('id');
        $ruta=rutas::find($id);
        return view('forms.edit-ruta',compact('ruta'));
    }
    public function destroy_ruta(Request $request){
        $id=$request->input('id');
        $ruta=rutas::find($id);
        $ruta->delete();
        return back()->with('success','Eliminado correctamente.');
    }

    public function actualizar_ruta(Request $request){
        $id_ruta=$request->input("id_ruta");
        $nombre=$request->input("nombre");
        $longitud_ruta=$request->input("longitud_total");
        $tiempo_total=$request->input("tiempo_total");
        $origen=$request->input("origen");
        $destino=$request->input("destino");
        $id_secciones=$request->input("id_seccion");
        $nombre_secciones=$request->input("nombre_seccion");
        $tiempo_secciones=$request->input("tiempoDesdeOrgen");
        $latitud_seccion=$request->input("ubicacion");
        $req="error";
        try {
            $ruta=rutas::find($id_ruta);
            $ruta->update([
                "nombre_ruta"=>$nombre,
                "longitud_ruta"=>$longitud_ruta,
                "tiempo_recorrido_total"=>$tiempo_total,
                "origen"=>$origen,
                "destino"=>$destino,
                "id_secciones"=>$id_secciones,
                "nombre_secciones"=>$nombre_secciones,
                "tiempo_secciones"=>$tiempo_secciones,
                "latitud_seccion"=>$latitud_seccion,
            ]);
            $req="success";
        } catch (Exception $e) {
            $req="error";
        }
        return json_encode($req);
    }

    public function guardar_ruta(Request $request){
        
        $nombre=$request->input("nombre");
        $longitud_ruta=$request->input("longitud_total");
        $tiempo_total=$request->input("tiempo_total");
        $origen=$request->input("origen");
        $destino=$request->input("destino");
        $id_secciones=$request->input("id_seccion");
        $nombre_secciones=$request->input("nombre_seccion");
        $tiempo_secciones=$request->input("tiempoDesdeOrgen");
        $latitud_seccion=$request->input("ubicacion");
        $req="error";
        try {
            $ruta=rutas::create([
                "nombre_ruta"=>$nombre,
                "longitud_ruta"=>$longitud_ruta,
                "tiempo_recorrido_total"=>$tiempo_total,
                "origen"=>$origen,
                "destino"=>$destino,
                "id_secciones"=>$id_secciones,
                "nombre_secciones"=>$nombre_secciones,
                "tiempo_secciones"=>$tiempo_secciones,
                "latitud_seccion"=>$latitud_seccion,
            ]);
            $req="success";
        } catch (Exception $e) {
            $req="error";
        }
        return json_encode($req);
    }

 public function verCliente(Request $request){
   
        $id=$request->input('id');
        $cliente=clientes::find($id);
        $rutas_all=rutas::all();
        $id_ruta=$cliente->id_ruta;
        $rutas=rutas::find($id_ruta);
        $nombre_rutaFind=$rutas->nombre_ruta;
        // var_dump($rutas->nombre_ruta);
        return view('forms.edit-clientes',compact('cliente','rutas_all','nombre_rutaFind'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
  public function actualizar(Request $request){
         $id=$request->input('id');
        $cliente=clientes::find($id);

        $newpass="";
        if (is_null($request->input('password') ) ) {
            $newpass=$request->input('password_old');
        }else{
            $newpass=Hash::make($request['passsword']);
        }

        try {
            $cliente->update(['nombre'=>$request['nombre'],
                'apellidos'=>$request['apellidos'],
                'municipio'=>$request['municipio'],
                'localidad'=>$request['localidad'],
                'calle'=>$request['calle'],
                'telefono'=>$request['telefono'],
                'correo'=>$request['correo'],
                'usuarioApp'=>$request['usuarioApp'],
                'password'=> $newpass,
                'status'=>$request['status'],
                'foto'=>$request['foto'],
                'nivel'=>$request['nivel'],
                'calificacion'=>$request['calificacion'],
                'monedero'=>$request['monedero'],
                'identificacion'=>$request['identificacion'],
                'id_ruta'=>$request['id_ruta']
            ]
            );
             $msj="Modificado correctamente";
        } catch (Exception $e) {
            $msj="Error al modificar";   
        }
        
       return redirect('/clientes')->withInput()->with("success","$msj");
    }


        public function search(Request $request){
           
        $busqueda=$request->get('busqueda');
        $clientes=clientes::orderBy('id','DESC')
        ->nombre($busqueda)
        ->paginate(20);
        $rutas=rutas::paginate(20);

        return view('forms.clientes',compact('clientes','rutas'));
    }

    public function buscarClientes(Request $request){
        $busqueda=$request->get('busqueda');
        $accion=$request->get('accion');
        if($accion==1){
            $clientes=clientes::select("id","nombre","apellidos","municipio","localidad","calle","telefono","monedero","calificacion","nivel","foto")
                ->orWhere('nombre','LIKE', "%$busqueda%")
                ->orWhere('apellidos','LIKE', "%$busqueda%")
                ->orWhere('identificacion','LIKE', "%$busqueda%")
                ->get();
            }//fin en_input
            if($accion==2){
                $clientes=DB::table('clientes')
                ->select("id","nombre","apellidos","municipio","localidad","calle","telefono","monedero","calificacion","nivel","foto")
                ->orWhere('id','=', "$busqueda")
                ->get();
            }//fin if en_select
            if ($accion==3) {
                $clientes=clientes::select("id","nombre","apellidos","municipio","localidad","calle","telefono","monedero","calificacion","nivel","foto")
                ->orWhere('nombre','LIKE', "%$busqueda%")
                ->orWhere('apellidos','LIKE', "$busqueda%")
                ->orWhere('identificacion','=', "$busqueda")
                ->get();
            }if ($accion==4) {
                $clientes=clientes::find($busqueda);
            }

    return json_encode($clientes);

    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->all()!=null) {
            $id_ruta=1;
            if ($request->input('id_ruta')==null) {
               $id_ruta=1;
            }else{
                $id_ruta=$request->input('id_ruta');
            }
            clientes::create([
                'nombre'=>$request['nombre'],
                'apellidos'=>$request['apellidos'],
                'municipio'=>$request['municipio'],
                'localidad'=>$request['localidad'],
                'calle'=>$request['calle'],
                'telefono'=>$request['telefono'],
                'correo'=>$request['correo'],
                'usuarioApp'=>$request['usuarioApp'],
                'password'=>Hash::make($request['password']),
                'status'=>"disable",
                'foto'=>$request['foto'],
                'nivel'=>$request['nivel'],
                'calificacion'=>$request['calificacion'],
                'monedero'=>0.00,
                'identificacion'=>$request['identificacion'],
                'id_ruta'=>$id_ruta
        ]);
            return back()->with('success','Registro Creado correctamente.');
        }else{
            return back()->with('error','Error al guardar.');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $cliente=clientes::find($id);
         $cliente->delete();
        return back()->with('success','Eliminado correctamente.');
    }
}
