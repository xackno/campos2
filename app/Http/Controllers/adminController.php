<?php

namespace App\Http\Controllers;
use App\Models\provedores;
use App\Models\categorias;
use App\Models\lineas;
use App\Models\marcas;
use App\Models\locales;
use App\Models\productos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Models\configsystems;
class adminController extends Controller
{

use RegistersUsers;

public function alogin($id){
    $negocio=configsystems::find($id);
    session_start();
    session(['negocio' => $negocio->nombre_negocio,'logotipo'=>$negocio->logotipo,'id'=>$negocio->id,'columnas'=>$negocio->precios_producto]);
return view("auth.login",compact('negocio'));
}

public function configuracion(){
    $user=User::orderBy('users.id','DESC')
    ->join('locales', 'users.local', '=', 'locales.id')
    ->paginate(20);
    $locales=locales::all();
    return view('forms.configuracion',compact('user','locales'));
}

public function updateuser(Request $request){
    $id=$request->input('id');
    $user=User::find($id);
    $newpass="";
    if (is_null($request->input('password') ) ) {
        $newpass=$request->input('password_old');
    }else{
        $newpass=Hash::make($request['password']);//Hash::make
    }

    $respuesta="";
    try {
        $user->update([
            'name'=>$request['name'],
            'user'=>$request['user'],
            'email'=>$request['email'],
            'type'=>$request['type'],
            'local'=>$request['local'],
            'foto'=>$request['foto'],
            'password'=>$newpass,
        ]);
        $respuesta="success";
    } catch (Exception $e) {
        $respuesta="error";
    }
    return json_encode($respuesta);
}




//
public function buscarProducto(Request $request){
        $busqueda=$request->input('busqueda');

    $productos=productos::select('productos.id as id_p','clave','descripcion_articulo','unidad','precio_venta','precio_compra','mayoreo_1','mayoreo_2','mayoreo_3','mayoreo_4',
            'codigo_sat','provedores.nombres as nombre_provedor','categorias.nombre as nombre_categoria','lineas.nombre as nombre_linea','marcas.nombre as nombre_marca','existencia','ubicacion_producto','fotos','codigo_barra','caducidad','descripcion_catalogo','productos.updated_at')
        ->join('provedores', 'productos.proveedor', '=', 'provedores.id')
        ->join('categorias', 'productos.categoria', '=', 'categorias.id')
        ->join('lineas', 'productos.linea', '=', 'lineas.id')
        ->join('marcas', 'productos.marca', '=', 'marcas.id')
        ->orWhere('descripcion_articulo','LIKE', "%$busqueda%")
        ->orWhere('palabra_clave','LIKE', "%$busqueda%")
        ->orWhere('clave','LIKE', "%$busqueda%")
        ->orWhere('codigo_barra','LIKE', "%$busqueda%")
        ->orWhere('caducidad','LIKE', "%$busqueda%")
        ->orWhere('marca','LIKE', "%$busqueda%")
        ->get();
    
    return json_encode($productos);
}

public function buscarXcodigo(Request $request){
    $cod=$request->get('busqueda');
    $prod=productos::orWhere('codigo_barra','=', "$cod")
        ->get();
    return json_encode($prod);
}




    public function uploadlogotipo(Request $request){

        // $nombre="hola llego";
        // $request->file('imagen')->store('');
        $file = $request->file('imagen');
        if ($file!=null) {
            $nombre = $file->getClientOriginalName();
       \Storage::disk('system')->put($nombre,  \File::get($file));
            $status="success";
        }else{
            $status="fail";
        }
       
    
     return $response = array('nombre' => $nombre,'status'=>$status);
    }

    public function uploadavatar(Request $request){

        // $nombre="hola llego";
        // $request->file('imagen')->store('');
        $file = $request->file('imagen');
        if ($file!=null) {
            $nombre = $file->getClientOriginalName();
       \Storage::disk('avatar')->put($nombre,  \File::get($file));
            $status="success";
        }else{
            $status="fail";
        }
       
    
     return $response = array('nombre' => $nombre,'status'=>$status);
    }

    public function uploadImg(Request $request){

        // $nombre="hola llego";
        // $request->file('imagen')->store('');
        $file = $request->file('imagen');
        if ($file!=null) {
            $nombre = $file->getClientOriginalName();
       \Storage::disk('local')->put($nombre,  \File::get($file));
            $status="success";
        }else{
            $status="fail";
        }
       
    
     return $response = array('nombre' => $nombre,'status'=>$status);
    }

    public function uploadImgUser(Request $request){

        // $nombre="hola llego";
        // $request->file('imagen')->store('');
        $file = $request->file('imagen');
        if ($file!=null) {
            $nombre = $file->getClientOriginalName();
       \Storage::disk('avatar')->put($nombre,  \File::get($file));
            $status="success";
        }else{
            $status="fail";
        }
       
    
     return $response = array('nombre' => $nombre,'status'=>$status);
    }




    public function register()
    {
        $locales=locales::all();
        return view('auth.register',compact('locales'));
    }

    public function registrarUsuario(Request $data){
        date_default_timezone_set('America/Mexico_City');
         User::create([
            'name' => $data['name'],
            'user' => $data['user'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'type'=>$data['type'],
            'local'=>$data['local'],
            'foto'=>$data['foto']
        ]);
          return back()->with('success','Registro Creado correctamente.');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createProvedor(Request $request)
    {
        date_default_timezone_set('America/Mexico_City');
        if ($request['nombres'] == !null && $request['telefono'] ==!null && $request['direccion']==!null) {
            $this->validate($request,[ 'nombres'=>'required', 'telefono'=>'required','direccion'=>'required']);
            provedores::create($request->all());
            return back()->with('success','Registro Creado correctamente.');
        }else{
            return back()->with('error','Error al guardar.');
        }
    
    }
    public function createCategoria(Request $request)
    {
        date_default_timezone_set('America/Mexico_City');
       if ($request['nombre']==!null && $request['descripcion']==!null) {
            $this->validate($request,[ 'nombre'=>'required', 'descripcion'=>'required']);
            categorias::create($request->all());
            return back()->with('success','Registro Creado correctamente.');
        }else{
            return back()->with('error','Error al guardar.');
        }
    
    }

    public function createLinea(Request $request)
    {
        date_default_timezone_set('America/Mexico_City');
            if ($request['nombre']==!null && $request['descripcion']==!null) {
            $this->validate($request,[ 'nombre'=>'required', 'descripcion'=>'required']);
            lineas::create($request->all());
            return back()->with('success','Registro Creado correctamente.');
        }else{
            return back()->with('error','Error al guardar.');
        }
    
    }

    public function createMarca(Request $request)
    {
            date_default_timezone_set('America/Mexico_City');
        if ($request['nombre']==!null && $request['descripcion']==!null) {
            $this->validate($request,[ 'nombre'=>'required', 'descripcion'=>'required']);
            marcas::create($request->all());
            return back()->with('success','Registro Creado correctamente.');
        }else{
            return back()->with('error','Error al guardar.');
        }
        
    
    }

}
