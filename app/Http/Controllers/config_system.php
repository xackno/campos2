<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\configsystems;
use App\Models\conexion;
class config_system extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('system/create_system');
    }
    public function home(){
        $negocios=configsystems::on("default")->get();
        return view("boardsystem",compact('negocios'));
    }
    public function agregando_conexion($nombre_negocio){
        //####################################################################
        //#######AGREGANDO CONEXION EN EL ARCHIVO CONFIG################################
        $archivo = 'config/database.php';//Nombre de archivo
        $abrir = fopen($archivo,'r+');//abriendo el archivo
        $contenido = fread($abrir,filesize($archivo));//analiado el archivo
        fclose($abrir); 

        $contenido = explode("\n",$contenido);//convirtiendo el archivo en un arreglo por el salto de linea. 
        //agregando texto despues de la linea 18
        $contenido[18] = "\n\t\t\t".
                "'".$nombre_negocio."' => ["."\n\t\t\t\t". 
                    "'driver' => 'mysql',"."\n\t\t\t\t".
                    "'url' => env('DATABASE_URL'),"."\n\t\t\t\t".
                    "'host' => env('DB_HOST', '127.0.0.1'),"."\n\t\t\t\t".
                    "'port' => env('DB_PORT', '3306'),"."\n\t\t\t\t".
                    "'database' => ".'"'.$nombre_negocio.'"'.",\n\t\t\t\t".
                    "'username' => env('DB_USERNAME', 'forge'),"."\n\t\t\t\t".
                    "'password' => env('DB_PASSWORD', ''),"."\n\t\t\t\t".
                    "'unix_socket' => env('DB_SOCKET', ''),"."\n\t\t\t\t".
                    "'charset' => 'utf8mb4',"."\n\t\t\t\t".
                    "'collation' => 'utf8mb4_unicode_ci',"."\n\t\t\t\t".
                    "'prefix' => '',"."\n\t\t\t\t".
                    "'prefix_indexes' => true,"."\n\t\t\t\t".
                    "'strict' => true,"."\n\t\t\t\t".
                    "'engine' => null,"."\n\t\t\t\t".
                    "'options' => extension_loaded('pdo_mysql') ? array_filter(["."\n\t\t\t\t".
                       " PDO::MYSQL_ATTR_SSL_CA => env('MYSQL_ATTR_SSL_CA'),"."\n\t\t\t\t".
                    "]) : [],"."\n\t\t\t\t".
                "],"."\n\t\t\t\t"
         ;

        // Uniendo la posicion 18 por el areglo original
        $contenido = implode("\n",$contenido);
        // Guardar Archivo
        $abrir = fopen($archivo,'w');
        fwrite($abrir,$contenido);
        fclose($abrir);
    }
    function clonando_tablas($namedb,$precios_producto){
        $user=env('DB_USERNAME', 'forge');
        $pass=env('DB_PASSWORD', '');
        $mysqli = new \mysqli('localhost',"$user","$pass","$namedb");

        $clonado="fail";
        try {
            $clone1="CREATE TABLE categorias SELECT *FROM retail.categorias";
            $mysqli->query($clone1);   
            $clone2="CREATE TABLE clientes SELECT *FROM retail.clientes";
            $mysqli->query($clone2);   
            // $clone3="CREATE TABLE configsystems SELECT *FROM retail.configsystems";
            // $mysqli->query($clone3); 
            $clone4="CREATE TABLE lineas SELECT *FROM retail.lineas";
            $mysqli->query($clone4); 
            $clone5="CREATE TABLE locales SELECT *FROM retail.locales";
            $mysqli->query($clone5); 
            $clone6="CREATE TABLE marcas SELECT *FROM retail.marcas";
            $mysqli->query($clone6); 
            $clone7="CREATE TABLE ofertas SELECT *FROM retail.ofertas";
            $mysqli->query($clone7); 
            $clone8="CREATE TABLE pedidos SELECT *FROM retail.pedidos";
            $mysqli->query($clone8); 


            
            if ($precios_producto!="") { //si se esta reciviendo campo se agregan a la tabla de productos
                $precios_producto=explode(",", $precios_producto);
                $agregar="";

                $cont=count($precios_producto);
                for ($x=1;$x<$cont;$x++){
                    $nombreColumna=$precios_producto[$x];
                    $nombreColumna=explode("/", $nombreColumna);
                    $agregar=$agregar."`".$nombreColumna[0]."` double(8,2) NOT NULL,";
                }

                $crearproductos="CREATE TABLE IF NOT EXISTS `productos` (
                  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
                  `clave` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                  `descripcion_articulo` text COLLATE utf8mb4_unicode_ci NOT NULL,
                  `unidad` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                  ".$agregar."
                  `codigo_sat` int(11) DEFAULT NULL,
                  `proveedor` int(11) DEFAULT NULL,
                  `categoria` int(11) DEFAULT NULL,
                  `linea` int(11) DEFAULT NULL,
                  `marca` int(11) DEFAULT NULL,
                  `existencia` int(11) NOT NULL,
                  `local` int(11) NOT NULL,
                  `ubicacion_producto` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                  `fotos` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                  `palabra_clave` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                  `url` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                  `codigo_barra` bigint(20) NOT NULL,
                  `caducidad` date DEFAULT NULL,
                  `descripcion_catalogo` text COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
                  `created_at` timestamp NULL DEFAULT NULL,
                  `updated_at` timestamp NULL DEFAULT NULL,
                  PRIMARY KEY (`id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
                $mysqli->query($crearproductos);  
            }else{//si no se recide se toma el default,
                $clone9="CREATE TABLE productos SELECT *FROM retail.productos";
                $mysqli->query($clone9); 
            }

            $clone10="CREATE TABLE productos_pedidos SELECT *FROM retail.productos_pedidos";
            $mysqli->query($clone10); 
            $clone11="CREATE TABLE prod_vendidos SELECT *FROM retail.prod_vendidos";
            $mysqli->query($clone11); 
            $clone12="CREATE TABLE provedores SELECT *FROM retail.provedores";
            $mysqli->query($clone12); 
            $clone13="CREATE TABLE rutas SELECT *FROM retail.rutas";
            $mysqli->query($clone13); 
            $clone14="CREATE TABLE turnos SELECT *FROM retail.turnos";
            $mysqli->query($clone14); 
            $clone15="CREATE TABLE users SELECT *FROM retail.users";
            $mysqli->query($clone15); 
            $clone16="CREATE TABLE ventas SELECT *FROM retail.ventas";
            $mysqli->query($clone16); 
            $clonado="success";
          } catch (Exception $e) {
             $clonado="fail"; 
          }  
        return $clonado;
       
    }

    public function save(Request $request){
        $nombre_negocio=$request->input("nombre_negocio");
        $nombre_negocio=str_replace(" ", "_",$nombre_negocio);
        $nombre_negocio=str_replace("-", "_",$nombre_negocio);
        $descripcion=$request->input("descripcion");
        $ubicacion=$request->input("ubicacion");
        $logotipo=$request->input("logotipo");
        $nombre_responsable=$request->input("nombre_responsable");
        $apellidos=$request->input("apellidos");
        $number_phone=$request->input("number_phone");
        $email=$request->input("email");
        $precios_producto=$request->input("precios_producto");
        $req="error";
        $clonado="fail";

        // echo $clonado;
        
        //############creando la nueva BD para este negocio-------------------
            $user=env('DB_USERNAME', 'forge');
            $pass=env('DB_PASSWORD', '');
            $mysqli = new \mysqli('localhost',"$user","$pass");

            // $conexion = new PDO("mysql:host=localhost;", "xackno", "admin123");
            $bd="SHOW DATABASES";
            $result = $mysqli->query($bd);
            $ya_existe=0;
            //comprobando si ya existe esa BD 
             while($fila = $result->fetch_array()){
                if ($fila[0]==$nombre_negocio) {
                    $ya_existe=1;
                    }
                }
            if ($ya_existe==0) {//si aun no existe crear la BD y registrarlo en la tabla configsystems de la BD default
                $createdb="CREATE DATABASE $nombre_negocio";
                $mysqli->query($createdb);

                
                $this->agregando_conexion($nombre_negocio);//modificando el config/database.php para agregar mas conexiones a la BD.
                
                if ($this->clonando_tablas($nombre_negocio,$precios_producto)) {
                  $clonado="success";
                }
                
                try {
                    $configsystems=configsystems::on("default")->create([
                        "nombre_negocio"=>$nombre_negocio,
                        "descripcion"=>$descripcion,
                        "ubicacion"=>$ubicacion,
                        "logotipo"=>$logotipo,
                        "nombre_responsable"=>$nombre_responsable,
                        "apellidos"=>$apellidos,
                        "telefono"=>$number_phone,
                        "email"=>$email,
                        "precios_producto"=>$precios_producto
                    ]);
                    $req="success";
                } catch (Exception $e) {
                    $req="error";
                }//fin catch
            }//fin if

    
        
        return compact('req','clonado');
    }




    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
