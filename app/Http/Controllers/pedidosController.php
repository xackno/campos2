<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\pedidos;
use App\Models\productos_pedidos;
use App\Models\clientes;
use Illuminate\Support\Facades\DB;
class pedidosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pedidos=pedidos::select('pedidos.id','clientes.nombre','fecha_pedido','fecha_enviado','fecha_recibido','gps','ubicacion_actual','importe','subtotal','costo_envio','descuento','total','pedidos.status')
        ->join('clientes', 'pedidos.id_cliente', '=', 'clientes.id')
        ->paginate(20);
        return view('forms.pedidos',compact('pedidos'));
    }
    public function verPedido(Request $request){
      
        $id=$request->input('id');
        $pedido=pedidos::find($id);

        return view('forms.edit-pedidos',compact('pedido'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        
    }
    public function verProPedido(Request $request){
      
      $idPedido=$request->input('idPedido');
      $productos_pedidos=DB::select('SELECT productos.id,id_pedido,id_producto,productos_pedidos.cantidad,productos.unidad,descripcion_articulo,precio,importe,fotos 
         FROM productos_pedidos INNER JOIN productos ON productos_pedidos.id_producto = productos.id WHERE id_pedido=?',[$idPedido]);

      return json_encode($productos_pedidos);
    }


    public function actualizar(Request $request){
      
      $id=$request->input('id');
        $pedido=pedidos::find($id);
        $statusNuevo=$request->input('status');

        if ($statusNuevo=="guardado" ||$statusNuevo=="pendiente" || $statusNuevo=="revision" || $statusNuevo=="surtiendo" ) {
          $pedido->update([
            "gps"=>"no disponible",
            "cp"=>$request['cp'],
            "localidad"=>$request['localidad'],
            "colonia"=>$request['colonia'],
            "calle"=>$request['calle'],
            "numero_exterior"=>$request['numero_exterior'],
            "numero_interior"=>$request['numero_interior'],
            "descripcion_lugar_envio"=>$request['descripcion_lugar_envio'],
            "ubicacion_actual"=>$request['ubicacion_actual'],
            "costo_envio"=>$request['costo_envio'],
            "subtotal"=>$request['subtotal'],
            "total"=>$request['total'],
            "status"=>$request['status'],
        ]);

        }
        if ($statusNuevo=="enviado") {
          $pedido->update([
            "fecha_enviado"=>now(),
            "descripcion_lugar_envio"=>$request['descripcion_lugar_envio'],
            "ubicacion_actual"=>$request['ubicacion_actual'],
            "costo_envio"=>$request['costo_envio'],
            "status"=>$request['status'],
        ]);

        }
        if ($statusNuevo=="recibido") {
          $pedido->update([
            "fecha_recibido"=>now(),
            "subtotal"=>$request['subtotal'],
            "total"=>$request['total'],
            "status"=>$request['status'],
        ]);

        }
        $msj="Modificado correctamente";

      return view('forms.edit-pedidos',compact('pedido','msj'));
    }




    public function crearpedido(Request $request){
      
       $idCliente=$request->input("idCliente");
       $cp=$request->input("cp");
       $localidad=$request->input("localidad");
       $colonia=$request->input("colonia");
       $calle=$request->input("calle");
       $exterior=$request->input("exterior");
       $interior=$request->input("interior");
       $descripcion_lugar=$request->input("descripcion_lugar");
       $importe=$request->input("importe");
       $costo_envio=$request->input("costo_envio");
       $subtotal=$request->input("subtotal");
       $descuento=$request->input("descuento");
       $total=$request->input("total");

       //datos de los areglos de productos
       $idP=$request->input("idP");
       $cantidad=$request->input("cantidad");
       $unidad=$request->input("unidad");
       $descripcion=$request->input("descripcion");
       $precio=$request->input("precio");
       $subtotalProducto=$request->input("subtotalProducto");

       $status="";
       $estadoPedido="";
       $accion=$request->input("accion");
       if($accion=="guardar"){
        $estadoPedido="guardado";

       }if ($accion=="realizado") {
           $estadoPedido="pediente";
       }

       try {
        $pedidos=pedidos::create([
            "id_cliente"=>$idCliente,
            "fecha_pedido"=>now(),
            "gps"=>"no disponible",
            "cp"=>$cp,
            "localidad"=>$localidad,
            "colonia"=>$colonia,
            "calle"=>$calle,
            "numero_exterior"=>$exterior,
            "numero_interior"=>$interior,
            "descripcion_lugar_envio"=>$descripcion_lugar,
            "ubicacion_actual"=>"en almacen",
            "importe" => $importe,
            "costo_envio" => $costo_envio,
            "subtotal" => $subtotal,
            "descuento" => $descuento,
            "total" => $total,
            "status" => $estadoPedido,
        ]);
            $status="success";
       } catch (Exception $e) {
           $status="fail";
       }
       $idpedido=pedidos::select('id')->get();
        $ultimoP=$idpedido->last();
        $prodd="";

       if ($status=="success") {
        //#############restando en el monedero del cliente
        $cliente=clientes::find($idCliente);
            $monedero_cliente=$cliente->monedero;
            $nombreCompletoCliente=$cliente->nombre." ".$cliente->apellidos;
        $cliente=$cliente->update([
          "monedero"=>$monedero_cliente-$descuento
        ]);

        $cantProd=count($idP);
           try {
            for ($x=0; $x<$cantProd; $x++) { 
                $prodd=$prodd." ".$descripcion[$x];
                $pro_pedidos=productos_pedidos::create([
                "id_pedido"=> $ultimoP->id,
                "id_producto"=>$idP[$x],
                "cantidad"=>$cantidad[$x],
                "unidad"=>$unidad[$x],
                "precio"=>$precio[$x],
                "importe"=>$subtotalProducto[$x],
               ]);
            } //fin for
            $status="success"; 
           } catch (Exception $e) {
               $status="fail";
           }//fin catch
       }//fin if

       //###############################################################################################
        $contents = \Storage::disk("files")->get('imprimir.json');
        $proceso = json_decode($contents, true);
        $proceso=$proceso["proceso"]+1;

        $ticket_json=array(
          "proceso"=>$proceso,
          "tipo"=>"pedido",
          'cajero'=>$request->input("cajero"),
          "fecha"=>$request->input("fecha"),
          'pedido'=>$ultimoP,

          "nombre_cliente"=>$nombreCompletoCliente,
          "cp"=>$cp,
          "localidad"=>$localidad,
          "colonia"=>$colonia,
          "calle"=>$calle,
          "numero_exterior"=>$exterior,
          "numero_interior"=>$interior,
          "descripcion_lugar_envio"=>$descripcion_lugar,
          "importe" => $importe,
          "costo_envio" => $costo_envio,
          "subtotal" => $subtotal,
          "descuento" => $descuento,
          "total" => $total,

          "desc_pro"=>$descripcion,
          "cantidad"=>$cantidad,
          "unidad"=>$unidad,
          "precio"=>$precio,
          "subtotalProducto"=>$subtotalProducto,
        );

        $json_venta=json_encode($ticket_json);


        \Storage::disk('files')->put("imprimir.json",$json_venta);



        return json_encode(compact('status','cantProd','prodd','descuento','importe')); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pedido=pedidos::find($id);
         $pedido->delete();
        return back()->with('success','Eliminado correctamente.');
    }
}
