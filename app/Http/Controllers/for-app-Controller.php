
<?php

  header('content-type: application/json; charset=utf-8');
header("access-control-allow-origin: *");



    if ($_GET){
        $action = $_GET["action"];
        if (function_exists($action)){
            call_user_func($action);
        }
    }
    function total_catalogo(){
	require("../../conexion.php");
	$request=new stdClass();

	$query2="SELECT * FROM productos";
	$result = $conexion->query($query2);
	$total = $result->num_rows;

	$request->total_registro=$total;
	echo json_encode($request);
}

function catalogo(){
	require("../../conexion.php");
	$id=$_POST['id'];
	$request=new stdClass();

	$query2="SELECT * FROM productos WHERE id ='$id'";
	$result = $conexion->query($query2);
	if($result->num_rows > 0){
		while($fila = $result->fetch_array()){
			$request->foto=$fila['fotos'];
			$request->unidad= $fila['unidad'];
			$request->descripcion_articulo= $fila['descripcion_articulo'];
			$request->precio_venta= $fila['precio_venta'];
		}
		$request->status="true";	
	}else{
		$request->status="fail";
	}
	

	echo json_encode($request);
}






function updatePedidos(){
	require("../../conexion.php");
	$id_pedido=$_POST['id_pedido'];
	$estado=$_POST['estado'];
	$request=new stdClass();

	$query2="UPDATE pedidos SET status='$estado', fecha_recibido=NOW() WHERE id='$id_pedido'";
	if($conexion->query($query2)){
		$request->status="true";	
	}else{
		$request->status="fail";
	}
	

	echo json_encode($request);
}

function getProductosPedidos(){
	require("../../conexion.php");
	$id_pedido=$_POST['id_pedido'];
	$request=new stdClass();

	$query2="SELECT * FROM productos_pedidos INNER JOIN productos ON productos_pedidos.id_producto=productos.id WHERE id_pedido ='$id_pedido'";
	$result = $conexion->query($query2);
	if($result->num_rows > 0){
		while($fila = $result->fetch_array()){
			$request->fotos[]=$fila['fotos'];
			$request->codigo_barra[]=$fila['codigo_barra'];
			$request->cantidad[]= $fila['cantidad'];
			$request->unidad[]= $fila['unidad'];
			$request->descripcion_articulo[]= $fila['descripcion_articulo'];
			$request->precio[]= $fila['precio'];
			$request->importe[]= $fila['importe'];
		}
		$request->status="true";	
	}else{
		$request->status="fail";
	}
	

	echo json_encode($request);
}

function getPedido(){
	require("../../conexion.php");
	$id_pedido=$_POST['id_pedido'];
	$request=new stdClass();

	$query2="SELECT *FROM pedidos WHERE id ='$id_pedido'";
	$result = $conexion->query($query2);
	if($result->num_rows > 0){
		while($fila = $result->fetch_array()){
			$request->id=$fila['id'];
			$request->fecha_enviado= $fila['fecha_enviado'];
			$request->fecha_recibido= $fila['fecha_recibido'];
			$request->total= $fila['total'];
			$request->estado= $fila['status'];
			$request->created_at= $fila['created_at'];
		}
		$request->status="true";	
	}else{
		$request->status="fail";
	}
	

	echo json_encode($request);
}
function listarPedidos(){
	require("../../conexion.php");
	$id_cliente=$_POST['id_cliente'];
	$request=new stdClass();

	$query2="SELECT *FROM pedidos WHERE id_cliente ='$id_cliente'";
	$result = $conexion->query($query2);
	if($result->num_rows > 0){
		while($fila = $result->fetch_array()){
			$request->id[]=$fila['id'];
			$request->fecha_enviado[]= $fila['fecha_enviado'];
			$request->fecha_recivido[]= $fila['fecha_recibido'];
			$request->total[]= $fila['total'];
			$request->estado[]= $fila['status'];
			$request->created_at[]= $fila['created_at'];
		}
		$request->status="true";	
	}else{
		$request->status="fail";
	}
	

	echo json_encode($request);
}

function pedidos(){
	require("../../conexion.php");

	$id_cliente=$_POST['id_cliente'];
	$id_art=$_POST['id_art'];
	$cantidad=$_POST['cantidad'];
	$unidad=$_POST['unidad'];
	$precio=$_POST['precio'];
	$importe=$_POST['importe'];
	$total=$_POST['total'];
	$request=new stdClass();


	 $sql="INSERT INTO pedidos (`id_cliente`, `total`, `status`, `created_at`) VALUES ('$id_cliente', '$total', 'nuevo', now())";
	
	 if($conexion->query($sql)===true ){
	 	 $ultimoPedido="SELECT MAX(id) FROM pedidos";
		 	$ultimoPed=0;
			$resultPedido=$conexion->query($ultimoPedido);
			while($fila = $resultPedido->fetch_array()){
			    $ultimoPed= $fila['MAX(id)'];
		       }
		       if ($ultimoPed==null) {
		       	$ultimoPed=0;
		       }
				 
		 for ($x=0; $x <count($id_art) ; $x++) { 
		 	$request->max_id=$ultimoPed;
		 	$queryProductos="INSERT INTO productos_pedidos 
		 	(`id_pedido`, `id_producto`, `cantidad`, `unidad`, `precio`, `importe`,created_at)
		 		 values('$ultimoPed','$id_art[$x]','$cantidad[$x]','$unidad[$x]','$precio[$x]','$importe[$x]',now())";
		 	$result2=$conexion->query($queryProductos);
		 }
		 $status="true";
	 }else{
	 	$status="false";
	 }

	 $request->status=$status;
	 
	echo json_encode($request);
}


function getProductos(){

	$request=new stdClass();
	$busqueda=$_POST['busqueda'];

	require("../../conexion.php");

	$query2="SELECT *FROM productos WHERE descripcion_articulo LIKE '%$busqueda%';";
	$result = $conexion->query($query2);
	if($result->num_rows > 0){
		while($fila = $result->fetch_array()){
			$request->id[]= $fila['id'];
			$request->descripcion_articulo[]= $fila['descripcion_articulo'];
			$request->precio_venta[]= $fila['precio_venta'];
			$request->unidad[]= $fila['unidad'];
			$request->fotos[]= $fila['fotos'];
			$request->codigo_barra[]= $fila['codigo_barra'];
			$request->codigo_sat[]= $fila['codigo_sat'];
		}
		$request->status="correcto";	
	}else{
		$request->status="sin registros";
	}
	

	echo json_encode($request);
}

function login(){
	require("../../conexion.php");
	$user=$_POST['user'];
	$pass=$_POST['password'];
	$password=$pass;
	$request=new stdClass();

	$query2="SELECT *FROM clientes WHERE usuarioApp='$user' AND password='$password'";
	$result = $conexion->query($query2);
	if($result->num_rows > 0){
		while($fila = $result->fetch_array()){
			$request->statusLogin="true";
			$request->id_cliente=$fila['id'];
			$request->usuarioApp=$fila['usuarioApp'];
			$request->status=$fila['status'];
		}
	}else{
		$request->statusLogin="false";
	}

	echo json_encode($request);
}



?>