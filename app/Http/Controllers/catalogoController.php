<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\provedores;
use App\Models\categorias;
use App\Models\lineas;
use App\Models\marcas;
use App\Models\locales;
use App\Models\productos;
use App\Models\configsystems;

class catalogoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
    	 $provedores=Provedores::all();
    	 $categorias=Categorias::all();
    	 $lineas=Lineas::all();
    	 $marcas=Marcas::all();
         
    	 $productos=productos::paginate(20);

        return view("forms.catalogo",compact('provedores','categorias','lineas','marcas','productos'));
    }

     public function search_for_catalogo(Request $request){

        $busqueda=$request->get('busqueda');
        $tipo=$request->get('tipo');
        $productos=productos::orderBy('id','DESC')
	        ->Descripcion_articulo($busqueda)
	        ->palabra_clave($busqueda)
	        ->clave($busqueda)
	        ->codigo_barra($busqueda)
	        ->paginate(20);

        if ($tipo=='general') {
        	$productos=productos::orderBy('id','DESC')
	        ->Descripcion_articulo($busqueda)
	        ->palabra_clave($busqueda)
	        ->clave($busqueda)
	        ->codigo_barra($busqueda)
	        ->paginate(20);
        }
        if($tipo=='provedor'){
        	$productos=productos::orderBy('id','DESC')
	        ->Proveedor($busqueda)
	        ->paginate(20);
        }
        if($tipo=='categoria'){
        	$productos=productos::orderBy('id','DESC')
	        ->categoria($busqueda)
	        ->paginate(20);
        }
        if($tipo=='marca'){
        	$productos=productos::orderBy('id','DESC')
	        ->marca($busqueda)
	        ->paginate(20);
        }
        if($tipo=='linea'){
        	$productos=productos::orderBy('id','DESC')
	        ->linea($busqueda)
	        ->paginate(20);
        }

        

        $provedores=provedores::all();
        $categorias=categorias::all();
        $marcas=marcas::all();
        $lineas=lineas::all();

        $id_negocio=session("id");
        $negocio=configsystems::find($id_negocio);
        //##########OBTENIENDO LOS NOMBRE DE LOS PRECIOS###################
        $nombreprecio=[];
        $permisos=[];
        $str_nombre="";
        $precios=explode(",",$negocio->precios_producto);
        foreach ($precios as $nombre) {
            $nombre2=explode("/",$nombre);
            if ($nombre!='') {
                array_push($nombreprecio,$nombre2[0]);
                array_push($permisos,$nombre2[1]);
            }
        }




        return view("forms.catalogo",compact('provedores','categorias','lineas','marcas','productos','nombreprecio','permisos'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
