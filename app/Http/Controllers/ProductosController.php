<?php

namespace App\Http\Controllers;


use App\Models\provedores;
use App\Models\categorias;
use App\Models\lineas;
use App\Models\marcas;
use App\Models\locales;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade as PDF;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\HttpFoundation\File\UploadedFileSplFileInfo;
use App\Imports\productosImport;
use App\Models\productos;
use Illuminate\Support\Facades\DB;
use App\Models\configsystems;

class ProductosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index(){
        date_default_timezone_set('America/Mexico_City');

        $productos=productos::
        selectRaw('productos.id as id_p,precio_compra,precio_venta,mayoreo_1,mayoreo_2,mayoreo_3,mayoreo_4, clave,descripcion_articulo,unidad,codigo_sat,provedores.nombres as nombre_provedor,categorias.nombre as nombre_categoria,lineas.nombre as nombre_linea,marcas.nombre as nombre_marca,existencia,ubicacion_producto,fotos,codigo_barra,caducidad,descripcion_catalogo')
        ->join('provedores', 'productos.proveedor', '=', 'provedores.id')
        ->join('categorias', 'productos.categoria', '=', 'categorias.id')
        ->join('lineas', 'productos.linea', '=', 'lineas.id')
        ->join('marcas', 'productos.marca', '=', 'marcas.id')
        ->get();
        $provedores=provedores::all();
        $categorias=categorias::all();
        $marcas=marcas::all();
        $lineas=lineas::all();
        $locales=locales::all();
      
        return view('forms.productos',compact('productos','provedores','categorias','lineas','marcas','locales'));
    }


    public function updateExistencia(Request $request){
        $id_p=$request->get('id_p');
        $entradas=$request->get('entradas');
        date_default_timezone_set('America/Mexico_City');

        $date=getdate();

        $status='';

        for($x=0;$x<count($id_p);$x++){
            try {
                $producto=productos::find($id_p[$x]);
                $exis_antes=$producto->existencia;
                $producto->update([
                    'existencia'=>$exis_antes+$entradas[$x],
                    'updated_at'=>now()
                ]);
                $status="success";

            } catch (Exception $e) {
                $status="error";
            }
            
        }

        return json_encode($status);
    }



    public function search(Request $request){
       
        $busqueda=$request->get('busqueda');

        $id_negocio=session("id");
        $negocio=configsystems::find($id_negocio);
        //##########OBTENIENDO LOS NOMBRE DE LOS PRECIOS###################
        $nombreprecio=[];
        $permisos=[];
        $str_nombre="";
        $precios=explode(",",$negocio->precios_producto);
        foreach ($precios as $nombre) {
            $nombre2=explode("/",$nombre);
            if ($nombre!='') {
                array_push($nombreprecio,$nombre2[0]);
                array_push($permisos,$nombre2[1]);
            }
        }
        $sqlPrecios=implode(",",$nombreprecio);
        //###################################################################
        // $productos=productos::select('productos.id as id_p','clave','descripcion_articulo','unidad',
        //     'codigo_sat','provedores.nombres as nombre_provedor','categorias.nombre as nombre_categoria','lineas.nombre as nombre_linea','marcas.nombre as nombre_marca','existencia','ubicacion_producto','fotos','codigo_barra','caducidad','descripcion_catalogo')
        $pre=productos::all();

        $productos=productos::
        selectRaw("$sqlPrecios".',productos.id as id_p,clave,descripcion_articulo,unidad,codigo_sat,provedores.nombres as nombre_provedor,categorias.nombre as nombre_categoria,lineas.nombre as nombre_linea,marcas.nombre as nombre_marca,existencia,ubicacion_producto,fotos,codigo_barra,caducidad,descripcion_catalogo')
        ->join('provedores', 'productos.proveedor', '=', 'provedores.id')
        ->join('categorias', 'productos.categoria', '=', 'categorias.id')
        ->join('lineas', 'productos.linea', '=', 'lineas.id')
        ->join('marcas', 'productos.marca', '=', 'marcas.id')

        ->orWhere('descripcion_articulo','LIKE', "%$busqueda%")
        ->orWhere('palabra_clave','LIKE', "%$busqueda%")
        ->orWhere('clave','LIKE', "%$busqueda%")
        ->orWhere('codigo_barra','LIKE', "%$busqueda%")
        ->orWhere('caducidad','LIKE', "%$busqueda%")
        ->orWhere('marca','LIKE', "%$busqueda%")
        ->paginate(20);





        // $productos=productos::select('productos.id as id_p','clave','descripcion_articulo','unidad','precio_venta',
        //     'codigo_sat','provedores.nombres as nombre_provedor','categorias.nombre as nombre_categoria','lineas.nombre as nombre_linea','marcas.nombre as nombre_marca','existencia','ubicacion_producto','fotos','codigo_barra','caducidad','descripcion_catalogo')
        // ->join('provedores', 'productos.proveedor', '=', 'provedores.id')
        // ->join('categorias', 'productos.categoria', '=', 'categorias.id')
        // ->join('lineas', 'productos.linea', '=', 'lineas.id')
        // ->join('marcas', 'productos.marca', '=', 'marcas.id')
        // ->orWhere('descripcion_articulo','LIKE', "%$busqueda%")
        // ->orWhere('palabra_clave','LIKE', "%$busqueda%")
        // ->orWhere('clave','LIKE', "%$busqueda%")
        // ->orWhere('codigo_barra','LIKE', "%$busqueda%")
        // ->orWhere('caducidad','LIKE', "%$busqueda%")
        // ->orWhere('marca','LIKE', "%$busqueda%")
        // ->paginate(20);

        $provedores=provedores::all();
        $categorias=categorias::all();
        $marcas=marcas::all();
        $lineas=lineas::all();
        $locales=locales::all();
        return view('forms.productos',compact('productos','provedores','categorias','lineas','marcas','locales','nombreprecio'));
    }




    public function loadExcel(Request $request){
        $file=$request->file('archivo');
         
         if (Excel::import(new productosimport, $file)) {
             return back()->with('message',"Importación exitosa");
         }else{
            return back()->with('error',"Error al importar verifique el archivo.");
         }
        
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        date_default_timezone_set('America/Mexico_City');
        if ($request->all()!=null) {
            productos::create($request->all());
            return back()->with('success','Producto guardado correctamente.');
        }else{
            return back()->with('error','Error al guardar.');
        }
    }


    public function verProducto(Request $request){
        date_default_timezone_set('America/Mexico_City');
        $id=$request->input('id');
        $producto=productos::find($id);
        $provedores=provedores::all();
        $categorias=categorias::all();
        $marcas=marcas::all();
        $lineas=lineas::all();
        $locales=locales::all();

        $id_negocio=session("id");
        $negocio=configsystems::find($id_negocio);
        $nombreprecio=[];
        $precios=explode(",",$negocio->precios_producto);
        foreach ($precios as $nombre) {
            if ($nombre!='') {
                $nombre=explode("/",$nombre);
                array_push($nombreprecio,$nombre[0]);
            }
            
        }



        $msj="";
        return view('forms.edit-productos',compact('producto','provedores','categorias','lineas','marcas','locales','msj','nombreprecio'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\productos  $productos
     * @return \Illuminate\Http\Response
     */
    public function actualizar(Request $request)
    {
        date_default_timezone_set('America/Mexico_City');
        $id=$request->input('id');
        $producto=productos::find($id);
        
        if ($producto->update($request->all())) {
            $msj="Modificado correctamente";
        }else{
            $msj="Error al modificar";
        }

     
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\productos  $productos
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      // delete
        session_start();
        $producto=productos::find($id);
        $producto->delete();
        return back()->with('success','Eliminado correctamente');
    }
}
