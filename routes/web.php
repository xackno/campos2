<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('auth.login');
// });
route::get('/','catalogoController@index')->name('catalogo');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');









Route::get('/reg','adminController@register')->name('reg');
Route::get('/home', 'HomeController@index')->name('home');
Route::post('/registrarUsuario','adminController@registrarUsuario')->name('registrarUsuario');
//router para el catalogo
Route::get('/catalogo', 'catalogoController@index')->name('catalogo');
Route::get('/search_for_catalogo','catalogoController@search_for_catalogo')->name('productos.search_for_catalogo');
Route::post('/uploadavatar','adminController@uploadavatar');
Route::post('/uploadlogotipo','adminController@uploadlogotipo');


//para cofiguracion de otro negocio
Route::get("/nuevo",'config_system@index');
Route::post("/guardar_negocio",'config_system@save')->name("guardar_negocio");

Route::get("/alogin/{datos}","adminController@alogin")->name("alogin");


Route::middleware(['auth','verified'])->group(function () {


	Route::get('/configuracion', 'adminController@configuracion')->name('configuracion');
	Route::post('/updateuser', 'adminController@updateuser')->name('updateuser');

	///ruta para la vista de ruta---------------------
	Route::get("/rutas",'clientesController@rutas')->name('rutas');
	Route::post("/guardar_ruta","clientesController@guardar_ruta")->name('guardar_ruta');
	Route::post("/actualizar_ruta","clientesController@actualizar_ruta")->name('actualizar_ruta');
	Route::post("/see_ruta","clientesController@see_ruta")->name('see_ruta');
	Route::post("/edit_ruta","clientesController@edit_ruta")->name('edit_ruta');
	Route::post("/destroy_ruta","clientesController@destroy_ruta")->name('destroy_ruta');

	//------------------para pedidos--------------------------
	Route::post('/verPedido', 'pedidosController@verPedido')->name('verPedido');
	Route::get('/pedidos', 'pedidosController@index')->name('pedidos');
	Route::get('/paraPedidos', 'ventasController@paraPedidos')->name('paraPedidos');
	Route::post('/crearpedido', 'pedidosController@crearpedido')->name('crearpedido');
	Route::post("actualizar-pedido",'pedidosController@actualizar')->name('actualizar-pedido');
	Route::post("/verProdutosPedido",'pedidosController@verProPedido')->name("verProdutosPedido");

		//----------------ROUTER EN HISTORIAL-------------------------------------
		Route::get('/historial', 'historialController@index')->name('historial');
		Route::post('/Lista_vendidos', 'historialController@list_ajax')->name('Lista_vendidos');


		Route::resource('productos','ProductosController');
		Route::resource('ventas','ventasController');
		Route::resource('ofertas','ofertasController');
		Route::resource('clientes','clientesController');
		Route::resource('pedidoss','pedidosController');
		
		//ALTA A PROVEDORES-----------------
		Route::get('/storeProvedor','adminController@createProvedor')->name('storeProvedor');
		Route::get('/storeCategoria','adminController@createCategoria')->name('storeCategoria');
		Route::get('/storeLinea','adminController@createLinea')->name('storeLinea');
		Route::get('/storeMarca','adminController@createMarca')->name('storeMarca');
		Route::get('/storeProductos','ProductosController@store')->name('storeProducto');
		//--------------para agregar una nueva oferta
		Route::get('/crearOferta','ofertasController@createOferta')->name('crearOferta');

		Route::post('/uploadImg','adminController@uploadImg');
		Route::post('/uploadImgUser','adminController@uploadImgUser');
		
		Route::post('/buscarProducto','adminController@buscarProducto')->name('buscarProducto');
		Route::post('/actualizarExistencia',"ProductosController@updateExistencia")->name('actualizarExistencia');

		Route::post('/buscarXcodigo','adminController@buscarXcodigo')->name('buscarXcodigo');

		//-___________-rutas para exportar e importar_________________________-__
		Route::post('/productos-import-loadExcel','ProductosController@loadExcel')->name('productos.import.loadExcel');
		Route::get('/DescargarExcel', function () {
		    return Excel::download(new productoExport, 'productos_de_retail.xlsx');
		});
		//##################################################################################
		// Route::post('import-list-excel', 'UserController@importExcel')->name('users.import.excel');
		// Route::post('productos/delete','ProductosController@destroy')->name('deleteProductos');
		Route::get('productos-search','ProductosController@search')->name('productos.search');
		Route::get('clientes-search','clientesController@search')->name('clientes.search');
		
		Route::post('/verProducto','ProductosController@verProducto')->name('verProducto');
		Route::post("/vercliente",'clientesController@verCliente')->name('vercliente');

		Route::post("productos-actualizar",'ProductosController@actualizar')->name('actualizar');
		Route::post("actualizar-cliente",'clientesController@actualizar')->name('actualizar-cliente');
		Route::get('/clientes','clientesController@index')->name('clientes.index');

		//##############routes para ventas############################################
		Route::post("/venta",'ventasController@ventas');
		Route::post("/buscarClientes",'clientesController@buscarClientes')->name("buscarClientes");
});







