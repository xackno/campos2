


$("#findProducto").keydown(function(e){ 
  if(e.which == 39){ //pusar flecha derecho
    buscarPro();
  }
});

$("#tabla_venta").on("keyup", "tr td:nth-child(2)", function(e) { //al modificar la cantidad
    var cantidad = $(this).html();
    var precio = $(this).siblings("td").eq(4).html();
    precio=precio.replace("$","");
    precio=parseFloat(precio);
    var subtotal =precio*parseFloat(cantidad);
    $(this).siblings("td").eq(5).html("$"+subtotal.toFixed(2));
    calcularTotal();
});
$("#tabla_venta").on("keyup", "tr td:nth-child(5)", function(e) { //al modificar la precio
    var precio = $(this).html();
    precio=parseFloat(precio);
    $(this).siblings("td").eq(4).html("$"+precio.toFixed(2));
    var cantidad = $(this).siblings("td").eq(1).html();
    var subtotal = parseFloat(cantidad) * precio;
    $(this).siblings("td").eq(5).html("$"+subtotal.toFixed(2));
    calcularTotal();
});





////#######################AGREGAR CON CODIGO DE BARRAS####### TAMBIEN PARA BASCULA ETIQUETADORA##############
$("#findProducto").keydown(function(e){ 
  var producto=$("#findProducto").val();
  if (e.which==13) {
    if (buscar_x_codigo(producto)!="") {///si sí encontro el producto
        var id=buscar_x_codigo(producto)[0]["id"];
        var unidad=buscar_x_codigo(producto)[0]["unidad"];
        var desc=buscar_x_codigo(producto)[0]["descripcion_articulo"];
        var precio=buscar_x_codigo(producto)[0]["precio_venta"];

        add_registro_tabla_venta(id,1,unidad,desc,precio,precio);
        $("#findProducto").val("");
    }else{//si no encontro el producto buscar ahora por el codigo de la vascula
      
      var c=producto.split("");
      var codigo=c[0]+c[1]+c[2]+c[3]+c[4]+c[5]+c[6];
      var peso=c[7]+c[8]+"."+c[9]+c[10];
      if (buscar_x_codigo(codigo)!="") {
        var id=buscar_x_codigo(codigo)[0]["id"];
        var unidad=buscar_x_codigo(codigo)[0]["unidad"];
        var desc=buscar_x_codigo(codigo)[0]["descripcion_articulo"];
        var x_unidad=buscar_x_codigo(codigo)[0]["precio_venta"];

        peso=parseFloat(peso);
        var precio=parseFloat(x_unidad);
        precio=peso*precio;
        precio=precio.toFixed(2);
        add_registro_tabla_venta(id,peso,unidad,desc,precio,x_unidad);
        $("#findProducto").val("");
      }else{
        alert("No se encontró el articulo");
      }
    }

    
  }
});





function add_registro_tabla_venta(id,cantidad,unidad,desc,precio,x_unidad){
    if (ya_existe(id,cantidad) == 0) {

        $("#tabla_venta").append("<tr tabindex='0' class='move'>  <td class='d-non'>" + id + "</td> <td contenteditable=''>"+cantidad+"</td> <td>" + unidad + "</td> <td class='text-primary text-left' style='text-transform: uppercase;'>" + desc + "</td> <td contenteditable='true'>" + precio + "</td> <td >$" + x_unidad + "</td> <td class='text-right'>$" + precio + "</td> <td onclick='eliminarPRO();'><i class='fas fa-trash text-danger'></i></td> </tr>");
        $("#findProducto").val('');
        $("#findProducto").focus();
        $("#start0").blur();
        calcularTotal();
    }
}

function ya_existe(id,cant) {
    var yaExiste = 0;
    $("#tabla_venta").find("tr td:first-child").each(function() {
        var id_art = $(this).html();
        var precio = $(this).siblings("td").eq(4).html();


        if (id == id_art) {
            precio=precio.replace("$","");
            precio=parseFloat(precio);
            yaExiste = 1;
            var cantidad = $(this).siblings("td").eq(0).html();
            cantidad=parseFloat(cantidad);
            $(this).siblings("td").eq(0).html(cantidad+cant);
            cantidad=cantidad+cant;
            var subtotal = cantidad * precio;
            subtotal=subtotal.toFixed(2);
           
            $(this).siblings("td").eq(5).html("$"+subtotal);
            calcularTotal();
            $("#findProducto").focus();
        }
    }); //fin each
    return yaExiste;
}


//##########################agregar a la tabla ventas con doble click#########################
$("#tb_busqueda").on("dblclick", "tr", function() {
    
    var id = $(this).find("td").eq(0).html();
    var unidad = $(this).find("td").eq(2).html();
    var desc = $(this).find("td").eq(3).html();
    var precio = $(this).find("td").eq(4).html();
    precio=parseFloat(precio);
    precio=precio.toFixed(2);

    add_registro_tabla_venta(id,1,unidad,desc,precio,precio);
    
    $("#ModalBusqueda").modal("hide");
    limpiartablabusqueda();
    iniciarFocus("start0");

});




function agregando_a_tabla_venta(start){
     $("#ModalBusqueda").modal("hide");
    var id = $(start).siblings("td").eq(0).html();
    var unidad = $(start).siblings("td").eq(2).html();
    var desc = $(start).html();
    var precio = $(start).siblings("td").eq(3).html();
    precio=parseFloat(precio);
    precio=precio.toFixed(2);
    iniciarFocus("start0");
    limpiartablabusqueda();
 
    add_registro_tabla_venta(id,1,unidad,desc,precio,precio);
    
}




function calcularTotal() {
    total = 0;
    $("#tabla_venta").find("tr td:nth-child(7)").each(function() {
        var subtotal=$(this).html();
        subtotal=subtotal.replace("$","");
        total += parseFloat(subtotal);
    });
    total = total.toFixed(2);
    $("#totalpagar").html(total);
}


//##############ELIMINAR PRODUCTOS DE LA TABLA#####################
var eliminarPRO = function() {
    $("#tabla_venta").on("click", "tr td:last-child", function() {
        var a = $(this).parent();
        $(a).remove();
        calcularTotal();
    });
}


//####################bloquear el enter en un td#############################
$('#tabla_venta').bind("keypress", "tr td", function(e) {
    if (e.which == 13) {
        $("#findProducto").focus();
        return false;
    }
});
$("body").keyup(function(e) { ///al presionar la tecla ESC enfocar al buscardor
        if (e.keyCode == 27) {
            $(".modal").modal("hide");
            $("#findProducto").focus();
        }
    });

$('#ModalBusqueda').on('shown.bs.modal', function() { //al lanzar el modal iniciar focus en la tabla de busqueda
    var id_elem = "start0";
    iniciarFocus(id_elem);
});


//#########################funcion limpiar tabla##################
    function limpiartablabusqueda() {
        var contEach = 0;
        start = document.getElementById('start0');
        start.focus();
        start.style.backgroundColor = 'green';
        start.style.color = 'white';
        document.onkeydown = checkKey;
        $("#tabla_busqueda tbody").find("tr td:first-child").each(function() {
            if (contEach > 0) {
                var a = $(this).parent();
                $(a).remove();
            } //fin if
            contEach++;
        }); //fin each
    }
//#########################funcion limpiar tabla##################
    function limpiartablaventa() {
        var contEach = 0;
        start = document.getElementById('td_focus');
        start.focus();
        start.style.backgroundColor = 'green';
        start.style.color = 'white';
        document.onkeydown = checkKey;
        $("#tabla_venta tbody").find("tr td:first-child").each(function() {
            if (contEach > 0) {
                var a = $(this).parent();
                $(a).remove();
            } //fin if
            contEach++;
        }); //fin each
    }











$("#findProducto").on("keyup", function(e) { //enfocar en tabla venta
    if (e.keyCode == 38) { //flecha arriba
        var id_elem = "td_focus";
        iniciarFocus2(id_elem);
    }
});










//#######################recorer tabla venta con flechas##########################################
var start2;

function iniciarFocus2(id_elem) {
    start2 = document.getElementById(id_elem);
    start2.focus();
    document.onkeydown = checkKey2;
}

function dotheneedful2(sibling) {
    if (sibling != null) {
        start2.focus();
        start2.style.backgroundColor = '';
        start2.style.color = '';
        sibling.focus();
        sibling.style.backgroundColor = 'green';
        sibling.style.color = '#fff';
        start2 = sibling;
    }
}

function checkKey2(e) {
    e = e || window.event;
    if (e.keyCode == '38') { //flecha arriba
        var idx2 = start2.cellIndex;
        var nextrow2 = start2.parentElement.previousElementSibling;
        if (nextrow2 != null) {
            var sibling2 = nextrow2.cells[idx2];
            dotheneedful2(sibling2);
        } else {
            dotheneedful2(start2);
            $("#findProducto").focus();
        }
    } else if (e.keyCode == '40') { //flecha abajo
        var idx2 = start2.cellIndex;
        var nextrow2 = start2.parentElement.nextElementSibling;
        if (nextrow2 != null) {
            var sibling2 = nextrow2.cells[idx2];
            dotheneedful2(sibling2);
        } else {
            dotheneedful2(start2);
            $("#findProducto").focus();
        }
    } else if (e.keyCode == '46') { //tecla suprimir un producto
        // a=$(this).parent();
        $(start2).parent().remove();
        calcularTotal();
        $("#findProducto").focus();
    }
} //fin checkkey


























//##########################focus para tb-busqueda##########################################  
var start;

function iniciarFocus(id_elem) {
    start = document.getElementById(id_elem);
    start.focus();
    document.onkeydown = checkKey;
}
//#############FUNCION PARA RECORER LA TABLA DE BUSQUEDA CON LAS FLECHAS#######################
function dotheneedful(sibling) {
    if (sibling != null) {
        start.focus();
        start.style.backgroundColor = '';
        start.style.color = '';
        sibling.focus();
        sibling.style.backgroundColor = 'green';
        sibling.style.color = '#fff';
        start = sibling;
    }
}

function checkKey(e) {
    e = e || window.event;
    if (e.keyCode == '13') {
        if ($(start).html() != '') {
            agregando_a_tabla_venta(start);
        }
    }
    if (e.keyCode == '38') { //flecha arriba
        var idx = start.cellIndex;
        var nextrow = start.parentElement.previousElementSibling;
        nextrow.scrollIntoView();
        if (nextrow != null) {
            var sibling = nextrow.cells[idx];
            dotheneedful(sibling);
        }
    } else if (e.keyCode == '40') { //flecha abajo
        var idx = start.cellIndex;
        var nextrow = start.parentElement.nextElementSibling;
        nextrow.scrollIntoView();
        if (nextrow != null) {
            var sibling = nextrow.cells[idx];
            dotheneedful(sibling);
        }
    }
} //fin checkkey