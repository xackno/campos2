@extends('layouts.app')
@section('content')

<!--window modal ######modal clientes################-->
  <div class="modal fullscreen-modal fade" id="modal_clientes" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content" >
      	<div class="modal-header text-light" style="background-color: #28a745;" >
      		<h1><i class="fa fa-plus"></i> Cliente <i class="app-menu__icon fa fa-user"></i></h1>
      		<span id="imgParaPerfil"></span>
      	</div>
        <div class="modal-body">
        	<div id="div_alert2"></div>
        	<form action="{{route('clientes.store')}}" method="POST">
        		@csrf
        		<div class="row">

				    <div class="col">
						<label for="nombre">Nombre</label>
		        		<input type="text" name="nombre" class="form-control" placeholder="Nombre o nombres ">
		        		<label for="apellidos">Apellidos</label>
		        		<input type="text" name="apellidos" class="form-control" placeholder="apellidos">
						<label for="municipio">Municipio</label>
						<input type="text" name="municipio" class="form-control" placeholder="municipio o C.P.">
		        		<label for="localidad">Localidad</label>
		        		<input type="text" name="localidad" class="form-control" placeholder="Localidad">
						<label for="calle">Calle</label>
		        		<input type="text" name="calle" class="form-control" placeholder="Nombre y número">
				    </div>

				    <div class="col">

				      	<label for="telefono">teléfono</label>
		        		<input type="tel" name="telefono" class="form-control" >

		        		<label for="correo">Correo</label>
		        		<input type="email" name="correo" class="form-control">

						<label for="usuarioApp">Usuario Para la App</label>
		        		<input type="text" name="usuarioApp" class="form-control">

						<label for="password">Contraseña</label>
		        		<input type="password" name="password"  class="form-control">

		        		<label for="foto">Foto</label>
		        		<input type="text" name="foto" id="foto" class="form-control" readonly="" value="default.png">
				 </div>

			</div>
        		
		       	<br><br>
	        	<button type="submit" class="btn btn-success" style="float: right;">Guardar</button>
        	</form>

        	<form enctype="multipart/form-data" id="formsubirFoto" method="post">
        		<label for="imagen"><b>Subir foto:</b></label>
			  <input type="file" name="imagen" id="imagen" />
			  <button  type="submit" class="btn btn-primary" id="subirImg">subir</button>
        	</form>
		  


        </div>
      </div>
    </div>
  </div>











@endsection
@section('script')
<script type="text/javascript">

</script>
@endsection