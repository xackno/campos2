@extends('layouts.app')
@section('content')
<div class="container" >
	<div id="div_alert"></div>
		@if(isset($msj))
		<div class="alert alert-primary alert-dismissible fade show">
			<h3>{{$msj}}</h3>
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			    <span aria-hidden="true">&times;</span>
			</button>
		</div>
		@endif
	
	    <div class="row justify-content-center">
	        <div class="col-md-12">
	        	
	            <div class="card" >
	            	<div class="card-header" style="background-color:#20c997">
						<h4 class="text-white" style="width: 100%"><b> Pedido</b> {{$pedido->id}}
							<span class="float-right text-white">Estado: {{$pedido->status}}</span>
						</h4>
	            		
	            	</div>
	            	<div class="card-body">
	            		<form action="{{route('actualizar-pedido')}}" method="POST">
			        		@csrf
	            		<div class="row">
	            			<div class="col-xl-3 col-md-6 mb-4">
	            				<label><b>Id</b></label>
	            				<input type="number" name="id" readonly="" class="form-control" value="{{$pedido->id}}">

	            				<label class="text-danger">Fecha de pedido</label>
	            				<input type="datetime" name="fecha_pedido" value="{{$pedido->fecha_pedido}}" class="form-control" readonly="">

	            				<label class="text-success">Fecha de enviado </label>	            				 
	            				<input type="datetime" name="fecha_enviado" value="{{$pedido->fecha_enviado}}" class="form-control" readonly="">

	            				<label class="text-info">Fecha de recibido</label>
	            				<input type="datetime" name="fecha_recibido" value="{{$pedido->fecha_recibido}}" class="form-control" readonly="">
	            			</div>
	            			<div class="col-xl-3 col-md-6 mb-4">
	            				<label class="text-warning">Ubicación del cliente</label>
	            				<input type="text" name="gps" value="{{$pedido->gps}}" class="form-control" readonly="" >

	            				<label>C.P.</label>
	            				<input type="number" name="cp" value="{{$pedido->cp}}" readonly="" class="form-control">
	            				<label>Localidad</label>
	            				<input type="text" name="localidad" value="{{$pedido->localidad}}" readonly="" class="form-control">
	            				<label>Colonia</label>
	            				<input type="text" name="colonia" value="{{$pedido->colonia}}" readonly="" class="form-control">
	            				<label>Calle</label>
	            				<input type="text" name="calle" value="{{$pedido->calle}}" readonly="" class="form-control">
	            			</div>
	            			<div class="col-xl-3 col-md-6 mb-4">
	            				<label>#. Exterior</label>
	            				<input type="text" name="numero_exterior" value="{{$pedido->numero_exterior}}" readonly="" class="form-control">
	            				<label>#. Interior</label>
	            				<input type="text" name="numero_interior" value="{{$pedido->numero_interior}}" readonly="" class="form-control">
	            				<label>Descripción</label>
	            				<textarea name="descripcion_lugar_envio" class="form-control">{{$pedido->descripcion_lugar_envio}}</textarea>
	            				<label class="text-primary">Ubicación del producto</label>
	            				<input type="text" name="ubicacion_actual" value="{{$pedido->ubicacion_actual}}" class="form-control">
	            				
	            				<label class="text-danger"><b>Cambiar status</b></label>
	            				<select class="form-control" name="status">
	            					<option value="{{$pedido->status}}" selected="true">{{$pedido->status}} -Seleccionado</option>
	            					<option value="guardado">guardado</option>
	            					<option value="pendiente">pendiente</option>
	            					<option value="revision">revisión</option>
	            					<option value="surtiendo">surtiendo</option>
	            					<option value="enviado">enviado</option>
	            					<option value="recibido">recibido</option>
	            				</select>
	            				
	            			</div>

	            			<div  class="col-xl-3 col-md-6 mb-4">
	            				<label class="text-success">Importe $</label>
	            				<input type="number" name="importe" class="form-control" value="{{$pedido->importe}}" readonly="">

	            				<label class="text-success">Envio $</label>
	            				<input type="number" name="costo_envio" class="form-control" value="{{$pedido->costo_envio}}" readonly="">

	            				<label class="text-success">Subtotal $</label>
	            				<input type="number" name="subtotal" class="form-control" value="{{$pedido->subtotal}}" readonly="">
	            				<label class="text-success">Descuento$</label>
	            				<input type="number" name="descuento" class="form-control" value="{{$pedido->descuento}}" readonly="">

	            				<label class="text-success">Total $</label>
	            				<input type="number" name="total" class="form-control" value="{{$pedido->total}}" readonly="">


	            				
	            			</div>

	            		</div>
	            		<button type="submit" class="btn btn-primary float-right">Guardar cambios</button>
	            	</form>
	            	<button class="btn btn-success" id="VerProductos"><i class="fa fa-eye"></i> Ver productos</button>
	            		
						




	           		</div>
	        </div> 
	            <!-- fin card -->
	        </div>

	    </div>
	</div>



<div class="modal" tabindex="-1" id="modal_pro_pedido" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header bg-info text-white">
        <h5 class="modal-title id_modal"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<p class="text-success">Productos de este pedido</p>
      		<div class="table-responsive" style="overflow-y: scroll;height: 400px">
			  <table class="table table-striped" id="tab_modal_pedido">
			    <thead class="table-dark">
			    	<tr>
			    		<th class="d-none">id</th>
			    		<th>Cantidad</th>
			    		<th>Unidad</th>
			    		<th>Descripción</th>
			    		<th>Precio</th>
			    		<th>total</th>
			    		<th>Foto</th>
			    	</tr>
			    </thead>
			    <tbody id="tb_modal_pedido">
			  	
			  </tbody>
			  </table>
			</div>
      </div>
      <div class="modal-footer" style="background: #efefef">
      	<button class="btn btn-success" disabled="">Guardar cambios</button>
      </div>

    </div>
  </div>
</div>





@endsection
@section('script')
<script type="text/javascript">

$("#VerProductos").click(function(){
	$("#modal_pro_pedido").modal("show");
	$.ajax({
		url:'{{route("verProdutosPedido")}}',
		type:'POST',
		dataType:'json',
		data:{
			idPedido:'{{$pedido->id}}'
		},success:function(data){
			for(var x=0;x<data.length;x++){
			// alert(data[x].id);
			var foto=data[x].fotos;
			foto=foto.split(',');
			$("#tb_modal_pedido").append('<tr>  <td class="d-none">'+data[x].id
				+' </td> <td>'+data[x].cantidad
				+'</td> <td>'+data[x].unidad
				+'</td><td>'+data[x].descripcion_articulo
				+'</td><td>$'+data[x].precio
				+'</td> <td>'+data[x].importe
				+'</td> <td><img src="public/img/productos/'+foto[0]+'" style="width:50px"></td></tr>');
			}
		},error:function(){

		}

	});
});

//-------------------------------------------------------
</script>
@endsection