@extends('layouts.app')
@section('content')
<div class="container" >
	<div id="div_alert"></div>
		@if(session('success'))
		<div class="alert alert-primary alert-dismissible fade show">
			<h3>{{session('success')}}</h3>
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			    <span aria-hidden="true">&times;</span>
			</button>
		</div>
		@endif
		@if(session('error'))
		<div class="alert alert-danger alert-dismissible fade show">
			<h3>{{session('error')}}</h3>
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			    <span aria-hidden="true">&times;</span>
			</button>
		</div>
		@endif




	    <div class="row justify-content-center">
	        <div class="col-md-12">
	        	
	            <div class="card" >
	            	<div class="card-header" style="background-color:#20c997">
	            		
	            		<span id="Perfil" class="float-left"><img src="public/img/avatar/{{$cliente->foto}}" class="imgPerfil" ></span>
	            		<p class="text-light" style="float: right;"><b> Cliente {{$cliente->id}}</b></p>
	            		<span id="span_star" class="float-right">
	            			@for($x=0;$x<5;$x++)
	            				@if($x<$cliente->calificacion)
	            					<i class="fa fa-star text-warning"></i>
	            				@else
									<i class="fa fa-star"></i>
	            				@endif
	            			@endfor
                    	</span>
	            	</div>
	            	<div class="card-body">
							<style type="text/css">
							.imgPerfil{
								
								position: absolute;float: right;
								width:60px;
							    height:62px;
							    border-radius:140px;
							    border:2px solid #666;
							}
							</style>
	            		<div id="div_alert2"></div>
			        	<form action="{{route('actualizar-cliente')}}" method="POST">
			        		@csrf
			        		<input type="hidden" name="id" value="{{$cliente->id}}">
			        		<div class="row">

							    <div class="col-xl-4 col-md-6 mb-4">
									<label for="nombre">Nombre</label>
					        		<input type="text" name="nombre" class="form-control" value="{{$cliente->nombre}}"><br>
					        		<label for="apellidos">Apellidos</label>
					        		<input type="text" name="apellidos" class="form-control" value="{{$cliente->apellidos}}"><br>
									<label for="municipio">Municipio</label>
									<input type="text" name="municipio" class="form-control" value="{{$cliente->municipio}}"><br>
					        		<label for="localidad">Localidad</label>
					        		<input type="text" name="localidad" class="form-control" value="{{$cliente->localidad}}">
					        		<br>
					        		<label class="text-success"><b>Monedero<i class="	fas fa-money-check-alt"></i> </b></label>
					        		<input type="number" name="monedero" class="form-control"  value="{{$cliente->monedero}}" readonly="">
									
							    </div>
							    <div class="col-xl-4 col-md-6 mb-4">
							    	<label for="calle">Calle</label>
					        		<input type="text" name="calle" class="form-control" value="{{$cliente->calle}}"><br>

					        		<label for="telefono">teléfono</label>
					        		<input type="tel" name="telefono" class="form-control" value="{{$cliente->telefono}}"><br>

					        		<label for="correo">Correo</label>
					        		<input type="email" name="correo" class="form-control" value="{{$cliente->correo}}"><br>

					        		<label for="foto" class="text-danger">Foto <i class="fab fa-algolia"></i></label>
					        		<input type="text" name="foto" id="foto" class="form-control" readonly="" value="{{$cliente->foto}}"><br>

					        		<label class="text-primary">Código de identificación <i class="fas fa-barcode"></i></label>
					        		<input type="number" name="identificacion" class="form-control" value="{{$cliente->identificacion}}" readonly="">
							    	
							    </div>

							    <div class="col-xl-4 col-md-6 mb-4">

									<label for="usuarioApp" class="text-warning">Usuario Para la App <i class="fas fa-user-circle"></i></label>
					        		<input type="text" name="usuarioApp" class="form-control" value="{{$cliente->usuarioApp}}"><br>

									<label for="password">Cambiar contraseña</label>
					        		<input type="text" name="password"  class="form-control" value="" placeholder="Nueva contraseña">

					        		<input type="text" name="password_old"  class="d-none" value="{{$cliente->password}}" >
					        		<br>
					        	<label>Tipo de cliente</label>
				        		@auth
		                      		@if(Auth::user()->type=='superadmin' || Auth::user()->type=='admin')
				        			<select class="form-control" name="nivel">
				        			<option value="{{$cliente->nivel}}" selected="true">{{$cliente->nivel}}</option>
				        			<option value="comun">común</option>
				        			<option value="Mayoreo1">Mayoreo 1</option>
				        			<option value="Mayoreo2">Mayoreo 2</option>
				        			<option value="Mayoreo3">Mayoreo 3</option>
				        			<option value="Mayoreo4">Mayoreo 4</option>
				        		</select>
				        		@else
				        			<select class="form-control" name="nivel">
				        			<option value="{{$cliente->nivel}}">{{$cliente->nivel}}</option>
				        		</select>
				        		@endif
		                    	@endauth
		                    	<br>

                    	<label>Calificáción <i class="fas fa-star text-warning"></i></label>
                    	<input type="number" name="calificacion" class="form-control" value="{{$cliente->calificacion}}" maxlength="1" max="5" min="1">
                    	<label>Rutas</label>
                    	<select class="form-control" name="id_ruta">
                    		<option value="{{$cliente->id_ruta}}" selected="">{{$nombre_rutaFind}}</option>
                    		@foreach($rutas_all as $ruta)
                    		@if($ruta->id!=$cliente->id_ruta)
                    		<option value="{{$ruta->id}}">{{$ruta->nombre_ruta}}</option>
                    		@endif
                    		@endforeach
                    	</select>
					        		
		        		<br>
		        		<h3 class="text-danger">Estado</h3>
		        		<label> Deshabilitado / Habilitado</label>
		        		<br>
		        		<label class="switch">
					       @auth
                      			@if(Auth::user()->type=='superadmin' || Auth::user()->type=='admin')
									<input type="checkbox" id="estado_cliente" >
									  <span class="slider round"></span>
									</label>
									<input type="text" name="status" id="status" class="form-control d-none" value="{{$cliente->status}}" >
								@else
		        					<input type="checkbox" id="estado_cliente" readonly="" >
									  <span class="slider round"></span>
									</label>
									<input type="text" name="status" id="status" class="form-control d-none" value="{{$cliente->status}}" readonly="">
				        		@endif
		                    	@endauth
							 </div>
							 

						</div>
			        		
					       	<br><br>
				        	<button type="submit" class="btn btn-success" style="float: right;">Guardar</button>
			        	</form>

			        	<form enctype="multipart/form-data" id="formsubirFoto" method="post">
			        		@csrf
			        		<label for="imagen"><b>Subir foto:</b></label>
						  <input type="file" name="imagen" id="imagen" />
						  <button  type="submit" class="btn btn-primary" id="subirImg">subir</button>
			        	</form>
		  
	           		</div>
	        </div> 
	            <!-- fin card -->
	        </div>

	    </div>
	</div>

<style type="text/css">
	.switch {
			  position: relative;
			  display: inline-block;
			  width: 60px;
			  height: 34px;
			}

			/* Hide default HTML checkbox */
			.switch input {
			  opacity: 0;
			  width: 0;
			  height: 0;
			}

			/* The slider */
			.slider {
			  position: absolute;
			  cursor: pointer;
			  top: 0;
			  left: 0;
			  right: 0;
			  bottom: 0;
			  background-color: #ccc;
			  -webkit-transition: .4s;
			  transition: .4s;
			}

			.slider:before {
			  position: absolute;
			  content: "";
			  height: 26px;
			  width: 26px;
			  left: 4px;
			  bottom: 4px;
			  background-color: white;
			  -webkit-transition: .4s;
			  transition: .4s;
			}

			input:checked + .slider {
			  background-color: #2196F3;
			}

			input:focus + .slider {
			  box-shadow: 0 0 1px #2196F3;
			}

			input:checked + .slider:before {
			  -webkit-transform: translateX(26px);
			  -ms-transform: translateX(26px);
			  transform: translateX(26px);
			}

			/* Rounded sliders */
			.slider.round {
			  border-radius: 34px;
			}

			.slider.round:before {
			  border-radius: 50%;
			}
</style>








@endsection
@section('script')
<script type="text/javascript">


//#################PARA ACTIVAR Y DESACTIVAR UN USUARIO, PARA EL CHECKBOX####################
if($("#status").val()=="enable"){
	$("#estado_cliente").attr('checked',"true");
}
if($("#status").val()=="disable"){
	$("#estado_cliente").removeAttr('checked');
}

$('#estado_cliente').click(function(){
	if($("#estado_cliente").is(':checked')) {  
            $("#status").val("enable"); 
        } else {  
        	$("#status").val("disable");  
        } 
});



	//-----------------------------------------------------------------------------
	$("#formsubirFoto").on("submit", function(e){
	            e.preventDefault();
	            var f = $(this);
	            var formData = new FormData(document.getElementById("formsubirFoto"));
	            formData.append("dato", "valor");
	            //formData.append(f.attr("name"), $(this)[0].files[0]);
	            $.ajax({
	                url: "{{url('/uploadImgUser')}}",
	                type: "post",
	                dataType: "",
	                data: formData,
	                cache: false,
	                contentType: false,
		     processData: false
	            }).done(function(e){
					// alert(e.nombre);
					if (e.status=="success") {
						$("#foto").val(e.nombre);
						$("#Perfil").html('<img src="public/img/'+e.nombre+'" class="imgPerfil" >');
						$("#div_alert2").html("<div class='alert alert-success' role='alert'>IMAGEN Cargado correctamente.</div>");
						setTimeout(function(){
				        $( "#div_alert2").html('');
				        }, 3500);
					}else{
						$("#div_alert2").html("<div class='alert alert-danger' role='alert'>Error al cargar.</div>");
						setTimeout(function(){
				        $( "#div_alert2").html('');
				        }, 3500);
					}
		}).fail(function (jqXHR, exception) {
	                // Our error logic here
	                console.log(exception);
	            });
	        });


//-------------mostrar modales  agregar------------------------------------
	$("#btn_toggle").trigger("click");
//-------------------------------------------------------
</script>
@endsection