@extends('layouts.app')

@section('content')
<ol class="breadcrumb" style="padding: 0px;margin: 0px">
    <li class="breadcrumb-item active"><a href="./">Inicio</a></li>
    <li class="breadcrumb-item " aria-current="page">Productos</li>
</ol>
<div class="card">
	<div class="card-header bg-secondary text-white">
		<button class="btn btn-success" id="btn_nuevo-productos"><i class="app-menu__icon  fa fa-plus"></i>Productos </button>
		<button class="btn btn-warning" id="btn_nuevo-provedores"><i class="app-menu__icon  fa fa-truck"></i>Provedores </button>
		<button class="btn btn-danger" id="btn_nuevo-categorias"><i class="app-menu__icon fas fa-code-branch"></i> Categorias </button>
		<button class="btn btn-info" id="btn_nuevo-lineas"><i class="app-menu__icon  fa        fa-tags"></i> Lineas</button>
		<button class="btn btn-primary" id="btn_nuevo-marcas"><i class="app-menu__icon  	fab fa-google-wallet"></i> Marcas</button>
		<button class="btn btn-success" id="Prod_for_excel"><i class="app-menu__icon fas fa-file-excel"></i> Excel</button>

		<button class="btn btn-primary float-right" id="btn_existencia"><i class="app-menu__icon fas "></i> Existencia</button>
	
	</div>
	<div class="card-body">
		<!-- #################para alertas######################## -->
			<div >
				@if(Session('message'))
		            <div class="alert alert-success alert-dismissible fade show">
							<h3>{{ Session('message') }}</h3>
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							    <span aria-hidden="true">&times;</span>
							</button>
						</div>
		            @endif
		        	@if(session('success'))
						<div class="alert alert-success alert-dismissible fade show">
							<h3>{{session('success')}}</h3>
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							    <span aria-hidden="true">&times;</span>
							</button>
						</div>
					@endif
					@if(session('error'))
						<div class="alert alert-danger alert-dismissible fade show">
							<h3>{{session('error')}}</h3>
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							    <span aria-hidden="true">&times;</span>
							</button>
						</div>
					@endif
			</div>
		<!-- ##################################################### -->
		
      	<div class="input-group">
      		<a href="{{url('/DescargarExcel')}}" class="btn btn-success"><i class="fas fa-download"></i> <i class="fas fa-file-excel"></i></a>
      		@if(Request::path()=='productos-search')
	      		<a class="btn btn-primary float-right" href="{{ route('productos.index') }}">
		          <i class="fas fa-fw fa-eye "></i>
		          <span><b>Ver todos</b></span>
		        </a>
      		@endif
      	</div>


		<div class="table-responsive">
			<table class="table table-striped table-bordered " id="tabla_lista_productos">
				<thead class="bg-dark" >
					<tr>
						<th>clave</th>
						<th>descripción</th>
						<th>unidad</th>
						<th>precios</th>
						<th>SAT</th>
						<th><i class="fa fa-truck text-warning"></i></th>
				       <th><i class="fas fa-code-branch text-danger"></i></th>
				       <th><i class="fa fa-tags text-info"></i></th>
				       <th><i class="fab fa-google-wallet text-primary"></i></th>
						<th>existencia</th>
						<th class="text-center"><i class="fa fa-map-marker text-success"></i></th>
						<th>fotos</th>
						<th class="text-center"><i class="fa fa-barcode"></i></th>
						<th><i class="fas fa-cog"></i></th>
					</tr>
				</thead>
				<tbody>
					@foreach($productos as $producto)
					<tr>
						<td>{{$producto->clave}}</td>
						<td class="text-primary"><strong>{{$producto->descripcion_articulo}}</strong></td>
						<td>{{$producto->unidad}}</td>
						<td>
							<select class="form-control">
		              			@auth
		              			@if(Auth::user()->type=='administrador')
			                      <option value="{{$producto->precio_compra}}" class="text-primary">compra - ${{number_format($producto->precio_compra, 2, '.', ',') }}</option>
			                      <option value="{{$producto->precio_venta}}" class="text-primary">venta - ${{ number_format($producto->precio_venta, 2, '.', ',')}}</option>
			                     	<option value="{{$producto->mayoreo_1}}" class="text-primary">mayoreo 1 - ${{ number_format($producto->mayoreo_1, 2, '.', ',') }}</option>
			                     	<option value="{{$producto->mayoreo_2}}" class="text-primary">mayoreo 2 - ${{ number_format($producto->mayoreo_2, 2, '.', ',')}}</option>
			                     	<option value="{{$producto->mayoreo_3}}" class="text-primary">mayoreo 3 - ${{number_format($producto->mayoreo_3, 2, '.', ',')}}</option>
			                     	<option value="{{$producto->mayoreo_4}}" class="text-primary">mayoreo 4 - ${{number_format($producto->mayoreo_4, 2, '.', ',')}}</option>
			                     @else
			                     	<option value="{{$producto->precio_venta}}" class="text-primary">venta - ${{ number_format($producto->precio_venta, 2, '.', ',')}}</option>
			                     	<option value="{{$producto->mayoreo_1}}" class="text-primary">mayoreo 1 - ${{ number_format($producto->mayoreo_1, 2, '.', ',') }}</option>
			                     	<option value="{{$producto->mayoreo_2}}" class="text-primary">mayoreo 2 - ${{ number_format($producto->mayoreo_2, 2, '.', ',')}}</option>
			                     	<option value="{{$producto->mayoreo_3}}" class="text-primary">mayoreo 3 - ${{number_format($producto->mayoreo_3, 2, '.', ',')}}</option>
			                     	<option value="{{$producto->mayoreo_4}}" class="text-primary">mayoreo 4 - ${{number_format($producto->mayoreo_4, 2, '.', ',')}}</option>
			                     @endif
			                    @endauth
		              		</select>
						</td>
						<td>{{$producto->codigo_sat}}</td>
		              	<td class="text_pequenos">{{$producto->nombre_provedor}}</td>
		              	<td class="text_pequenos">{{$producto->nombre_categoria}}</td>
		              	<td class="text_pequenos">{{$producto->nombre_linea}}</td>
		              	<td class="text_pequenos">{{$producto->nombre_marca}}</td>
		              	<td class="text-right"><b>{{$producto->existencia}}</b></td>
		              	<td class="text_pequenos">{{$producto->ubicacion_producto}}</td>
		              	<td class="td_imagen">
		              		<span class="d-none">{{$producto->fotos}}</span>	
		              		@if($producto->fotos!="ninguno")
		              		<?php $fot=explode(',', $producto->fotos); ?>
								    <img src="img/productos/{{$fot[0]}}" class="imgProductos">
		              		@else
		              			<img src="img/productos/no-images.jpg" class="imgProductos">
		              		@endif
		              	</td>
		              	<td>{{$producto->codigo_barra}}</td>
		              	<td style="width: 8%">
		              		<button class="btn btn-sm btn-danger" 
		              			onclick="
		              			var cnfm=confirm('Desea eliminar el producto?');
		              			if (cnfm==true) {
		              				$('#form_eliminar{{$producto->id_p}}').submit();
		              			}
		              			"
		              			@auth
		              			@if(Auth::user()->type!='administrador')
		              			disabled="true"
		              			@endif
		              			@endauth
		              			>
		              			<i class="fa fa-trash" ></i>
		              		</button>

		              		<button class="btn btn-sm btn-success" 
		              		onclick="$('#edit{{$producto->id_p}}').submit();"
		              		@auth
		              			@if(Auth::user()->type!='administrador')
		              			disabled="true"
		              			@endif
		              			@endauth
		              		>
		              			<i class="fas fa-edit" style="font-size: 80%"></i>
		              		</button>
		              		<!-- ################################################################################################################# -->
		              		<form action="{{ route('productos.destroy',$producto->id_p) }}" id="form_eliminar{{$producto->id_p}}" method="post" class="btnAction d-none">
		              			 @csrf
     							 @method('DELETE')
		              			<button class="btn btn-sm btn-danger">
		              				
		              			</button>
		              		</form>
		              		<!-- ______________________________________________________________ -->
		              		<form action="{{route('verProducto') }}" method="post" class="btnAction d-none" id="edit{{$producto->id_p}}">
		              			@csrf
     							<input type="hidden" name="id" id="id_editar" value="{{$producto->id_p}}">
		              			<button class="btn btn-sm btn-success">
		              			</button>
		              		</form>
		              	</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>


















	</div>
</div><!-- ##################fin card#################### -->

<style type="text/css">
	.text_pequenos{font-size: 70%}
	.text_pequenos:hover{font-size: 100%}
	#tabla_lista_productos tbody tr td{ 
		padding: 5px;margin:0px;
		border: 0.2px solid #eee;
		border-bottom: 2px solid #66bb6a;
		 }
	#tabla_lista_productos thead tr th,#tabla_existencia thead tr th{ 
		padding:4px;
		padding-left: 3px;
		padding-right: 3px 
	}
	.imgProductos{
		width: 40px;height: auto;
	}
	#tabla_lista_productos_filter label{float: right;}
</style>




	 


<!--window modal ######modal altar pro excel################-->
  <div class="modal fullscreen-modal fade" id="pro_excel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog " role="document">
      <div class="modal-content" >
      	<div class="modal-header text-light" style="background-color: #009688;" >
      		<h3><i class="fa fa-plus"></i>Agregar por Excel  <i class="fas fa-file-excel text-white"></i> </h3>
      		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          		<span aria-hidden="true"><b> <i class="fas fa-window-close"></i></b></span>
          </button>
      	</div>
        <div class="modal-body">
        	<p class="text-justify">Descargar plantilla de ejemplo del archivo <span class="text-primary">.xlsx</span>, Tener en cuenta que los titulo de las columnas deben de esta en la primera fila y con el mismo nombre que a parecen en este archivo. <a href="public/ejemplo.xlsx"> DESCARGAR AQUÍ...<i class="fas fa-download"></i></a></p>
        	<form enctype="multipart/form-data"  action="{{route('productos.import.loadExcel')}}" method="post">
        		@csrf
        		<label class="text-success"><b>Subir archivo:</b></label>
			  <input type="file" name="archivo" id="archivo"  class="form-control" />
			  <br>
			  <hr>	
			  <button  type="submit" class="btn btn-primary float-right">Cargar <i class="fas fa-upload"></i></button>
        	</form>

		    



        </div>
      </div>
    </div>
  </div>



<!--window modal ######modal existencia################-->
  <div class="modal fullscreen-modal fade" id="modal_existencia" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog " role="document">
      <div class="modal-content" >
      	<div class="modal-header text-white bg-primary" >

      		<h5>Existencia de productos</h5>
      		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          		<span aria-hidden="true"><b> <i class="fas fa-window-close"></i></b></span>
          </button>
      	</div>
        <div class="modal-body" style="text-align: center">
        	<div style="width: 50%">
        			<div class="input-group">
        				<input type="text" id="buscador" class="form-control" placeholder="nombre del producto">
        				<button class="btn btn-success" id="search_existencia"><i class="fa fa-search"></i></button>
        			</div>
        	</div><br>
        	<table class="table table-bordered" id="tabla_existencia">
        		<thead class="bg-dark text-light">
        			<tr>
        				<th>#</th>
        				<th>Entradas</th>
        				<th>Existencia</th>
        				<th>Descripción</th>
        				<th>Update</th>
        			</tr>
        		</thead>
        		<tbody></tbody>
        	</table>
        </div>
        <div class="modal-footer">
        		<button class="btn btn-success" id="btn_save_existencia">Guardar</button>
        </div>
      </div>
    </div>
  </div>



<!--window modal ######modal imagenes################-->
  <div class="modal fullscreen-modal fade" id="modal_imagenes" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog " role="document">
      <div class="modal-content" >
      	<div class="modal-header text-dark" style="background-color: #ffc107;" >

      		<h1> imagenes <i class="app-menu__icon  fa fa-picture-o"></i></h1>
      		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          		<span aria-hidden="true"><b> <i class="fas fa-window-close"></i></b></span>
          </button>
      	</div>
        <div class="modal-body" style="text-align: center">

        </div>
      </div>
    </div>
  </div>




<!--window modal ######modal provedores################-->
  <div class="modal fullscreen-modal fade" id="modal_provedores" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content" >
      	<div class="modal-header text-dark" style="background-color: #ffc107;" >
      		<h1><i class="fa fa-plus"></i> Provedor <i class="app-menu__icon  fa fa-truck"></i></h1>
      	</div>
        <div class="modal-body">
        	<form action="{{route('storeProvedor')}}" method="get">
        		<label for="nombres">Nombre</label>
        		<input type="text" name="nombres" class="form-control" placeholder="nombre">
        		<label for="telefono">Telefono</label>
        		<input type="number" name="telefono" class="form-control" placeholder="Telefono con lada">
        		<label for="direccion">Dirección</label>
        		<input type="text" name="direccion" placeholder="Estado, Distrito, municipio, colonia, calle." class="form-control"><br><br>
        		<button type="submit" class="btn btn-success" style="float: right;">Guardar</button>
        	</form>
        </div>
      </div>
    </div>
  </div>

<!--window modal ######modal categoria################-->
  <div class="modal fullscreen-modal fade" id="modal_categorias" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content" >
      	<div class="modal-header text-light" style="background-color: #dc3545;" >
      		<h1><i class="fa fa-plus"></i> Categoria <i class="app-menu__icon  fa fa-code-fork"></i></h1>
      	</div>
        <div class="modal-body">
        	<form action="{{route('storeCategoria')}}" method="get">
        		<label for="nombres">Nombre</label>
        		<input type="text" name="nombre" class="form-control" placeholder="nombre">
        		<label for="descripcion">Descripción</label>
        		<textarea name="descripcion" id="descripcion" class="form-control"></textarea>
        		<br><br>
        		<button type="submit" class="btn btn-success" style="float: right;">Guardar</button>
        	</form>
        </div>
      </div>
    </div>
  </div>

<!--window modal ######modal linea################-->
  <div class="modal fullscreen-modal fade" id="modal_lineas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content" >
      	<div class="modal-header text-light" style="background-color: #17a2b8;" >
      		<h1><i class="fa fa-plus"></i> Linea <i class="app-menu__icon  fa        fa-tags"></i></h1>
      	</div>
        <div class="modal-body">
        	<form action="{{route('storeLinea')}}" method="get">
        		<label for="nombres">Nombre</label>
        		<input type="text" name="nombre" class="form-control" placeholder="nombre">
        		<label for="descripcion">Descripción</label>
        		<textarea name="descripcion" id="descripcion" class="form-control"></textarea>
        		<br><br>
        		<button type="submit" class="btn btn-success" style="float: right;">Guardar</button>
        	</form>
        </div>
      </div>
    </div>
  </div>

<!--window modal ######modal marca################-->
  <div class="modal fullscreen-modal fade" id="modal_marcas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content" >
      	<div class="modal-header text-light bg-primary" >
      		<h1><i class="fa fa-plus"></i> Marca <i class="app-menu__icon  fab fa-google-wallet"></i> </h1>
      	</div>
        <div class="modal-body">
        	<form action="{{route('storeMarca')}}" method="get">
        		<label for="nombres">Nombre</label>
        		<input type="text" name="nombre" class="form-control" placeholder="nombre">
        		<label for="descripcion">Descripción</label>
        		<textarea name="descripcion" id="descripcion" class="form-control"></textarea>
        		<br><br>

        		<button type="submit" class="btn btn-success" style="float: right;">Guardar</button>
        	</form>
        </div>
      </div>
    </div>
  </div>

<!--window modal ######modal productos################-->
  <div class="modal fullscreen-modal fade" id="modal_productos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content" >
      	<div class="modal-header text-light" style="background-color: #28a745;" >
      		<h1><i class="fa fa-plus"></i> Producto <i class="app-menu__icon fa fa-shopping-basket"></i></h1>
      		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          		<span aria-hidden="true"><b> <i class="fas fa-window-close"></i></b></span>
          </button>
      	</div>
        <div class="modal-body">
        	<div id="div_alert"></div>
        	<form action="{{route('storeProducto')}}" method="GET">
        		<div class="row">

				    <div class="col-xl-3 col-md-6 mb-4">
				<label for="clave">Clave</label>
        		<input type="text" name="clave" class="form-control" placeholder="clave ">
        		<label for="descripcion_articulo">descripción del producto</label>
        		<input type="text" name="descripcion_articulo" class="form-control" placeholder="descripción">
				<label for="unidad">unidad</label>
					<select name="unidad" id="unidad" class="form-control">
						<option>Pieza</option>
						<option>Kilogramo</option>
						<option>Litro</option>
						<option>Gramo</option>
						<option>Caja</option>
						<option>Paquete</option>
					</select>
					<label for="palabra_clave">Palabras clave</label>
					<input type="text" name="palabra_clave" class="form-control">

					<label for="codigo_barra">Código de barra</label>
					<input type="number" name="codigo_barra" class="form-control">
					<label for="fotos">Imagenes</label>
					<input type="text" name="fotos" id="fotos" class="form-control" readonly="true" value="ninguno">
			        		
				</div>

				<div class="col-xl-3 col-md-6 mb-4">
						<label>Precio de compra</label>
						<input type="number" name="precio_compra" class="form-control" required="">

						<label>precio de venta</label>
						<input type="number" name="precio_venta" class="form-control" required="">

						<label>precio mayoreo 1</label>
						<input type="number" name="mayoreo_1" class="form-control">

						<label>precio mayoreo 2</label>
						<input type="number" name="mayoreo_2" class="form-control">

						<label>precio mayoreo 3</label>
						<input type="number" name="mayoreo_3" class="form-control">

						<label>precio mayoreo 4</label>
						<input type="number" name="mayoreo_4" class="form-control">


		        		
				 </div>

				<div class="col-xl-3 col-md-6 mb-4">
					<label for="categoria">Categoria</label>
		        		<select name="categoria" class="form-control">
							@foreach($categorias as $categoria)
								<option value="{{$categoria->id}}">{{$categoria->nombre}}</option>
							@endforeach
						</select>
		        		<label for="linea">linea</label>
		        			<select name="linea" class="form-control">
							@foreach($lineas as $linea)
								<option value="{{$linea->id}}">{{$linea->nombre}}</option>
							@endforeach
						</select>
						<label for="marca">Marca</label>
		        			<select name="marca" class="form-control">
							@foreach($marcas as $marca)
								<option value="{{$marca->id}}">{{$marca->nombre}}</option>
							@endforeach
						</select>
						<label for="existencia">existencia</label>
		        		<input type="number" name="existencia" class="form-control" placeholder="existencia en tienda.">

						<label for="local">Local</label>
		        		<select name="local" class="form-control">
		        			@foreach($locales as $local)
		        			<option value="{{$local->id}}">{{$local->nombre}}-{{$local->descripcion}}</option>
		        			@endforeach
		        		</select>

		        		<label>Caducidad</label>
					<input type="date" name="caducidad" class="form-control">
				</div>
				<div class="col-xl-3 col-md-6 mb-4">
					<label for="ubicacion_producto">Ubicación del producto</label>
					<input type="text" name="ubicacion_producto" id="ubicacion_producto" class="form-control" readonly="true" style="font-size: 70%">
					<select class="col-3" id="pasillo">
						<option>Pasillo 0</option>
						<option>Pasillo 1</option>
						<option>Pasillo 2</option>
						<option>Pasillo 3</option>
						<option>Pasillo 4</option>
					</select>
					<select class="col-3" id="anden">
						<option>Anden 0</option>
						<option>Anden 1</option>
						<option>Anden 2</option>
						<option>Anden 3</option>
						<option>Anden 4</option>
					</select>
					<select class="col-3" id="vitrina">
						<option>Vitrina 0</option>
						<option>Vitrina 1</option>
						<option>Vitrina 2</option>
						<option>Vitrina 3</option>
						<option>Vitrina 4</option>
					</select>
					<br>

					<label for="codigo_sat">código SAT</label>
		        		<input type="number" name="codigo_sat" class="form-control" placeholder="Código SAT">

						<label for="proveedor">proveedor</label>
						<select name="proveedor" class="form-control">
							@foreach($provedores as $provedor)
								<option value="{{$provedor->id}}">{{$provedor->nombres}}</option>
							@endforeach
						</select>					

					

					<label>Descripción para el catálogo</label>
					<textarea name="descripcion_catalogo"></textarea>

				</div>
			</div>
        		
		       	<br><br>
	        	<button type="submit" class="btn btn-success" id="guardarProducto" style="float: right;">Guardar</button>
        	</form>

        	<form enctype="multipart/form-data" id="formuploadajax" method="post">
        		@csrf
        		<label for="imagen"><b>Subir imagen:</b></label>
			  <input type="file" name="imagen" id="imagen" />
			  <button  type="submit" class="btn btn-primary" id="subirImg">subir</button>
        	</form>
        <script type="text/javascript">
        	
        	
        </script>

		  


        </div>
      </div>
    </div>
  </div>

@endsection

@section('script')
<script type="text/javascript">
// $("#sidebar").toggle();
$(".btn_toggle").trigger("click");
	if ($(window).width() <= 360) {
	    $(".btn_toggle").trigger("click");
	}


    $("#tabla_lista_productos").DataTable({
        "order": [[ 0, 'desc' ]],
        "info":     false,
        "language": {
        "decimal": "",
        "emptyTable": "No hay información",
        "lengthMenu": "Mostrar _MENU_",
        "loadingRecords": "Cargando...",
        "processing": "Procesando...",
        "search": "Buscar:",
        "zeroRecords": "Sin resultados encontrados",
        "paginate": {
            "first": "Primero",
            "last": "Ultimo",
            "next": "Siguiente",
            "previous": "Anterior"
        }
    }
});







	function buscarExis(){
		$("#tabla_existencia tbody").html('');
		$.ajax({
			url:"{{route('buscarProducto')}}",
			type:"post",
			dataType:"json",
			data:{
				"_token": "{{ csrf_token() }}",
				busqueda:$("#buscador").val()
			},
			success:function(e){
				
				for(var x=0;x<e.length;x++){
					// alert(e[x].id_p);
					$("#tabla_existencia tbody").append("<tr><td>"+e[x].id_p+"</td> <td contenteditable='true'></td>  <td>"+e[x].existencia+"</td>  <td>"+e[x].descripcion_articulo+"</td>  <td>"+e[x].updated_at+"</td> </tr>");
				}
			}
		});
	}

//##############for existencia####################################
	$("#btn_existencia").click(function(){
		$("#modal_existencia").modal("show");
	});
	$("#search_existencia").click(function(){
		buscarExis();
	});
		//#################BLOQUEAR ENTER EN UN TD########################
		  $('#tabla_existencia tbody ').bind("keypress","tr td", function(e){
		    if (e.which == 13) {
		      return false;
		    }
		  });

	//############save existencia#################################################################
		$("#btn_save_existencia").click(function(){
			var id=[];
			var entradas=[];
			$("#tabla_existencia tbody").find("tr td:first-child").each(function(){
				var td_entradas=$(this).siblings("td").eq(0).html();
            if (td_entradas!='') {
              id.push($(this).html());
              entradas.push(td_entradas);
            }//fin if   
          });//fin each
			// alert(entradas);

			$.ajax({
				url:"{{route('actualizarExistencia')}}",
				type:"post",
				dataType:"json",
				data:{
					"_token": "{{ csrf_token() }}",
					id_p:id,
					entradas:entradas
				},success:function(e){
					// alert(e);
					if (e=="success") {
						alert("Se actualizó corectamente");
						buscarExis();
					}else{
						alert("Error al actualizar verifique la información proporcionada");
					}	
				},error:function(e){
					alert("Error al actualizar verifique la información proporcionada");
				}
			});

		});








	var imagen="";
	var cont=0;

$("#Prod_for_excel").click(function(){
	$("#pro_excel").modal("show");
});

//_______________________________click a  la imagen____________________
	$("table tbody .td_imagen").click(function(){
		// alert($(this).find("span").html());
		var imagenes=$(this).find("span").html();
		imagenes=imagenes.split(",");
		if (imagenes=='ninguno') {
			$("#modal_imagenes .modal-body").html("");
			$("#modal_imagenes .modal-body").append("<div style='width:100%'><img src='public/img/productos/no-images.jpg' style='width:100%;height:auto;'></div>");
			$("#modal_imagenes").modal('show');
		}
		if(imagenes!='ninguno'){
		$("#modal_imagenes .modal-body").html("");
		for(var x=0;x<imagenes.length;x++){
			$("#modal_imagenes .modal-body").append("<div style='width:100%'><img src='public/img/productos/"+imagenes[x]+"' style='width:100%;height:auto;'></div>");
		}
		$("#modal_imagenes").modal('show');
		
		}
	});
//-----------------------------------------------------------------------------
	$("#pasillo,#anden,#vitrina").on('change',function(){
		var ubicacion=$("#pasillo").val()+","+$("#anden").val()+","+$("#vitrina").val();
		$("#ubicacion_producto").val(ubicacion);
	});

	$("#guardarProducto").click(function(){
		cont=0;
	});

	$("#formuploadajax").on("submit", function(e){
	            e.preventDefault();
	            var f = $(this);
	            var formData = new FormData(document.getElementById("formuploadajax"));
	            formData.append("dato", "valor");
	            //formData.append(f.attr("name"), $(this)[0].files[0]);
	            $.ajax({
	                url: "{{url('/uploadImg')}}",
	                type: "post",
	                dataType: "",
	                data: formData,
	                cache: false,
	                contentType: false,
		     	processData: false
	            }).done(function(e){
					// alert(e.nombre);
					if (e.status=="success") {
						if (cont==0) {
							imagen=e.nombre;
						}else{
							imagen=imagen+","+e.nombre;
						}
						cont++;
						
						$("#fotos").val(imagen);
						$("#div_alert").html("<div class='alert alert-success' role='alert'>IMAGEN Cargado correctamente.</div>");
						setTimeout(function(){
				        $( "#div_alert").html('');
				        }, 3500);
					}else{
						$("#div_alert").html("<div class='alert alert-danger' role='alert'>Error al cargar.</div>");
						setTimeout(function(){
				        $( "#div_alert").html('');
				        }, 3500);
					}
		}).fail(function (jqXHR, exception) {
	                // Our error logic here
	                console.log(exception);
	            });
	        });

//------------para sliders------------------
	$("#mayoreo_1").change(function(){
		$("#span1").html($(this).val());
	});
	$("#mayoreo_2").change(function(){
		$("#span2").html($(this).val());
	});
	$("#mayoreo_3").change(function(){
		$("#span3").html($(this).val());
	});
	$("#mayoreo_4").change(function(){
		$("#span4").html($(this).val());
	});
//-------------mostrar modales  agregar------------------------------------
	$("#btn_toggle").trigger("click");
	$("#Prod_for_excel").click(function(){
		$("#pro_excel").modal('show');
	});
	$("#btn_nuevo-provedores").click(function(){
		$("#modal_provedores").modal('show');
	});
	$("#btn_nuevo-categorias").click(function(){
		$("#modal_categorias").modal('show');
	});
	$("#btn_nuevo-lineas").click(function(){
		$("#modal_lineas").modal('show');
	});
	$("#btn_nuevo-marcas").click(function(){
		$("#modal_marcas").modal('show');
	});
	$("#btn_nuevo-productos").click(function(){
		$("#modal_productos").modal('show');
	});
//-------------------------------------------------------
</script>
@endsection
