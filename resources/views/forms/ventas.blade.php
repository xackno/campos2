@extends('layouts.app')
@section('style') 
@endsection

@section('content')        
  <div class="card" style="margin-bottom: 0px;height: 79.5vh;">
    <div class="card-body" style="text-align: center;padding: 0px">
      @if(isset($msj))
          <p id="msj" class="d-none">{{$msj}}</p>
      @endif
      <div class="table-responsive" style="height:75vh;overflow-y: scroll;">


        <table class="table table-bordered table-striped" id="tabla_venta">
          <thead class="bg-danger">
            <tr>   
             <th>id</th>
             <th>cantidad</th>
             <th>unidad</th>
             <th>descripción</th>
             <th>precio</th>
             <th>X unidad</th>
             <th>SUBTOTAL</th> 
             <th><i class="fa fa-cog"></i></th>   
            </tr>
          </thead>
          <tbody>
            <tr  class="d-none" >
              <td></td>
              <td id="td_focus"></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>

          </tbody>
        </table>
		</div>
   </div>
  </div>

<!-- ########################FOOTER############################### -->
<div class="card">
  <div class="card-body bg-primary"  style="margin:0;padding:8px !important;">
    <div class="row">
      <div class="col-12 col-md-4 col-xl-4 col-lg-4">
        <div class="input-group">
            <input type="text" name="busqueda"  class="form-control bg-light border-1 small" placeholder="Buscar..." autocomplete="off" id="findProducto" autofocus="true"
            aria-label="Search" aria-describedby="basic-addon2" style="border-color: #3f51b5;">
          <button class="btn btn-success" id="btn_buscarProdVenta" onclick="buscarPro();">
              <i class="fas fa-search fa-sm"></i>
          </button>
        </div>
      </div>
      <div class="col-6 col-md-2 col-xl-2 col-lg-2">
          <button  class="btn btn-light func" disabled="" id="btn_devoluciones">
             <i class="zmdi zmdi-shopping-cart" style="color: white;"></i>
              Dev
          </button>
          <button  class="btn btn-success func" id="f9" onclick="mostrarModal_venta();">
            <i class="zmdi  zmdi-shopping-cart-plus" style="color: white;"></i>
            F9
          </button>
      </div>
      <div class="col-6 col-md-3 col-xl-3 col-lg-3 ">
        <div id="alert-footer"></div>
      </div>
      <div class="col-12 col-md-3 col-xl-3 col-lg-3">
        <span class="float-right" style="font-size: 140%"><b>TOTAL: $ <span id="totalpagar">0.00</span> MXN</b></span>
        <h5 id="ultimoCambio" class="text-danger"></h5> 
      </div>
    </div>
    <div class="row d-none d-sm-none d-md-block">
      <span class="text-dark"> Usuario:<span id="usuario">{{ Auth::user()->user }}</span></span>
       <span>Fecha:<span id="fecha"></span> </span> 
       <span class="text-dark">Hora: <span id="hora"></span> </span>
       <span>Turno:<span id="turno"></span> </span>  
    </div>
  </div>
</div>




<style type="text/css">
  #tabla_venta thead th, #tabla_busqueda thead th{
    height: 20px;
    padding:4px;
  }
  #tabla_venta tbody td{
    height: 20px !important;
    padding: 0 !important;
  }
  #tabla_venta tbody tr:hover{
    color:black !important;
    background: #EDBB99 !important;
  }
  #tb_busqueda tr:hover{
    color:white !important;
    background: #138D75 !important;
  }

</style>



















<!--window modal ######modal realizar pedidos################-->
  <div class="modal fullscreen-modal fade" id="modal_pedidos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header bg-info">
          <h5 class="text-white"><i class="fa fa-clipboard"></i> Realizar pedido</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
          <div class="alert alert-warning" style="padding:0px" >
            <p>SUGERENCIA:<br> Despúes de cada $500.00 de compra aplica descuento del <b>Monedero.</b></p>
          </div> 
          <div id="div_alert_pedidos" style="width: 100%;margin:5px"></div>
          <div class="row">
              <div class="col-xl-3 col-md-6 mb-4">
                <label><b>Pedido para el cliente con:</b></label>
                <label>Nombre</label>
                <input type="text" id="nombreParaPedido" class="form-control" readonly="">
                <label>Localidad</label>
                <input type="text" id="localidadParaPedido" class="form-control" readonly="">
                <label>Identificación</label>
                <input type="text" id="identificacionParaPedido" class="form-control" readonly="">
                <label>Monedero <b class="text-danger">$</b></label>
                <input type="number" id="monedero" class="form-control text-success" readonly="">
              </div>

              <div class="col-xl-5 col-md-6 mb-4">
                <h5 class="text-info">Opcional <button class="btn btn-primary btn-sm" type="button" data-toggle="collapse" data-target="#opcional" aria-expanded="false" aria-controls="opcional">
                  <i class="fas fa-arrow-circle-down" id="i-btn-opcional"></i></button></h5>

                  <div class="collapse" id="opcional">
                <label>C.P.</label>
                <input type="text" id="cp" class="form-control" required="">
                <label>Localidad</label>
                <input type="text" id="localidad" class="form-control" required="">
                <label>Colonia</label>
                <input type="text" id="colonia" class="form-control" required="">
                <label>Calle</label>
                <input type="text" id="calle" class="form-control" required="">
                <div class="input-group">
                  <label>#. Exterior</label>
                <input type="text" id="exterior" class="form-control" value="0">
                <label>#. Interior</label>
                <input type="text" id="interior" class="form-control" value="0">  
                </div>
              </div>
              <!-- fin collapse -->
              </div>

              <div class="col-xl-3 col-md-6 mb-4">
                <label>Descripcion del lugar</label>
                <textarea id="descripcion_lugar" style="height: 150px" required="" >Sin descripción</textarea>
                <br>
                <label>Costo de envio</label>
                <input type="number" id="costo_envio" class="form-control" required="" value="0">
                <br>

                <!-- ############################################################################## -->

                <h6 class="float-right" style="margin:0px"> <span class="text-success">IMPORTE:$</span> <span id="subtotal" class="text-danger"></span></h6><br>

                <h6 class="float-right" style="margin:0px"> <span class="">Envio:$</span> <span id="span_envio" class="text-danger"></span></h6>

                <h6 class="float-right" style="margin: 0px"> <span class="text-success">SUBTOTAL:$</span> <span id="totalPedido" class="text-danger"></span></h6>
                <!-- ////aqui esta el checkbox -->
                <div class="form-check" id="div_check">
                  <input type="checkbox" class="form-check-input " id="exampleCheck1" value="20">
                  <label class="form-check-label" for="exampleCheck1" style=" font-size: 70%">
                    <b>Aplicar descuento 
                      <span class="text-primary">$20.00</span>
                    </b>
                  </label>
                </div>
                
                <div class="text-center">
                  <hr>
                    <h4 class="text-primary">Total a Pagar:$ <span class="text-dark" id="total_a_pagar"></span> </h4>
                </div>
                
              </div>
          </div>
       
        </div><!-- fin modal-body -->
        <div class="modal-footer" style="background:#efefef">
          <button class="btn btn-warning" onclick="cancelar_pedido();" >Cancelar</button>
          <button class="btn btn-primary" onclick="guardarPedido(1);">Sólo guardar</button>
          <button class="btn btn-success float-right" onclick="guardarPedido(2);">Enviar</button>
          
        </div>
      </div>
    </div>
  </div>
<!--window modal ######modal busqueda################-->
  <div class="modal fullscreen-modal fade" id="ModalBusqueda" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content" style="height: 500px">
        <div class="modal-header bg-info">
          <h4>Búsqueda</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
        <div class="table_responsive" style="overflow-x: scroll;overflow-y: scroll;height: 400px">
          <table id="tabla_busqueda" editabled="" class="table table-striped table-bordered" >
               <thead class="table-dark">
                   <tr>
                      <th>Id</th>
                      <th>Clave</th>
                      <th >Unidad</th>
                      <th >Descripción</th>
                      <th>$ Precio</th>
                      <th>Exist</th>
                      <th><i class="fa fa-map-marker"></i> </th>
                   </tr>
               </thead>
               <tbody id="tb_busqueda" >
                  <tr  class="d-none" >
                    <td></td>
                    <td></td>
                    <td></td>
                    <td id="start0"></td>
                  </tr>
               </tbody>
           </table>   
        </div>
        </div>
      </div>
    </div>
  </div>
<!--window modal ######modal realizarVenta################-->
  <div class="modal fullscreen-modal fade" id="realizarVenta" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog" role="document" style="width: 50%">
      <div class="modal-content">
        <div class="modal-header bg-success" style="padding: 0 !important">
          <h4 style="padding:5px !important"><b class="text-white"><i class="fas fa-shopping-cart"></i> VENTAS</b></h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="padding: 0;padding-top: 30px;padding-right: 30px"> 
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
          <div id="alertas" style="width: 80%"></div>
          <div class="row">
            <div class="col">
              <h3 class="text-danger">Total:$ <span id="Total_en_moda_Venta" class="text-danger"></span></h3>
            </div>
            <div class="col">
              <input type="number" id="input_importe" class="form-control" placeholder="Importe" style="text-align: right;">
            </div>
          </div>
            <span style="font-size: 200%" class="text-success">Cambio:$ </span>
            <span id="cambio_modal_venta" style="font-size: 200%" class="text-danger"></span> 
        </div>
        <div class="card-footer">
          <button class="btn btn-primary float-right" style="display: none" onclick="venta_al_contado();" id="btn_submit_venta"><i class="fas fa-shopping-cart"></i> Realizar</button>
        </div>
      </div>
    </div>
  </div>
<!--window modal ######modal ver imgs################-->
  <div class="modal fullscreen-modal fade" id="modal_imagenes" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog " role="document">
      <div class="modal-content" >
        <div class="modal-header text-dark" style="background-color: #ffc107;" >
          <h1> imagenes <i class="app-menu__icon  fa fa-picture-o"></i></h1>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body" style="text-align: center">

        </div>
      </div>
    </div>
  </div>
<!--window modal ######modal mensaje modalselectCliente################-->
  <div class="modal fullscreen-modal fade" id="modalSelectCliente" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header bg-info text-white">
          <h4>Seleccione un cliente</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
          <div id="div_para_alert" style="width: 100%;margin:5px"></div>
         <label><i class="fa fa-user"></i> Buscar cliente</label>
         <div class="input-group col-xl-4 col-md-6 mb-4">  
          <input type="text" class="form-control bg-light border-1 small" placeholder="Buscar..." autocomplete="off" id="findClient" autofocus="true"
            aria-label="Search" aria-describedby="basic-addon2" style="border-color: #3f51b5;">
          <button class="btn btn-primary" id="btn_findClient" onclick="buscarClientes(1);">
              <i class="fas fa-search fa-sm"></i>
            </button>
          </div><!-- fin nput-group -->

          <div class="row">
              <div class="col-xl-3 col-md-6 mb-4 ">
              <label>Selecciones uno</label>
              <select class="form-control" id="econtrados"  >
              </select>
              <input type="number" id="input_id_cliente" class="d-none">
              <input type="text" id="input_nivel_cliente" class="d-none">
            </div>
            <div class="col-xl-3 col-md-6 mb-4 ">
              <label>Nombre</label>
              <input type="text" id="nombreClinte" class="form-control" readonly="">

              <label>Localidad</label>
              <input type="text" id="localidad2" class="form-control" readonly="">

              <label>Calificación</label>
              <p id="stars"></p>
            </div>

            <div class="col-xl-3 col-md-6 mb-4 ">
              <label>Teléfono <i class="fas fa-phone-square text-danger"></i></label>
              <input type="text" id="telefono" class="form-control " readonly="">

              <label>En monedero <i class="fas fa-money-check-alt text-success"></i></label>
              <input type="text" id="monedero_al_buscar_cliente" class="form-control text-danger" readonly="">

              <label>Nivel</label>
              <input type="text" id="nivel" class="form-control" readonly="">
            </div>
            <div class="col-xl-3 col-md-6 mb-4">
              <div style="width:100%;margin:20px">
                <img src="img/avatar/no-images.jpg" id="fotoCliente" style="width: 70%;border:2px solid #aeaeff;border-radius: 40px" >
              </div>
            </div>
    
          </div>
          
        </div><!-- fin modal-body -->
        <div class="modal-footer">
          <button class="btn btn-success float-right" id="btn_selectionClient">Selecionar</button>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')

<script type="text/javascript" src="../resources/js/for-ventas.js"></script>
<script type="text/javascript">


//---------------------funcion mostrar modalventa
  function mostrarModal_venta(){
    $("#realizarVenta").modal("show");
    $("#Total_en_moda_Venta").html($("#totalpagar").html());
    $('#realizarVenta').on('shown.bs.modal', function () {
      $('#input_importe').focus();
    });
  }
//##########################################################################
   $("#input_importe").on('keyup',function(e){

    var totalVenta=parseFloat($("#totalpagar").html());
    var importe=parseFloat($(this).val());  
    var cambio=importe-totalVenta;
    $("#cambio_modal_venta").html("$"+cambio.toFixed(2));
    if (e.keyCode==13) {
      if(cambio>=0){
        submitventa();
      }else{
        alert("ERROR, en el cambio");
      } 
    }
    if (cambio>=0) {
      $("#btn_submit_venta").show();
    }else{$("#btn_submit_venta").hide();}
   });

///////////////////tecla funciones ######################################
  $("body").keyup(function(e) {
      if (e.keyCode == 120) { //F9
        mostrarModal_venta();
      }
    });



//"#########################submit venta al contado"
  function submitventa(){
    var realizar=confirm("Desea realizar la venta?");
    if (realizar) {
      var con_ticket=confirm("Desea imprimir ticket?");
      if(get_data_table_venta()["id"].length>0){//si hay productos en la tabla venta
        $.ajax({
          url:"{{url('/venta')}}",
          type:'post',
          dataType:'json',
          data:{
            objeto:get_data_table_venta(),
            importe:$("#input_importe").val(),
            cajero:"{{Auth::user()->name}}",
            turno:1,
            con_ticket:con_ticket
          },
          success:function(e){
              limpiando_venta();
              $("#alert-footer").html('<div class="alert alert-success" style="padding:3px;margin:auto">Venta #'+e.id_venta+' correnta</div>');
              setInterval(function(){
                $("#alert-footer").html('');
              },5000);
          },error:function(){
            $("#alert-footer").html('<div class="alert alert-danger" style="padding:3px;margin:auto">ERROR</div>');
              setInterval(function(){
                $("#alert-footer").html('');
              },5000);
          }
        });
      }else{
        alert("no hay productos en la tabla de VENTA");
      }//fin else no hay productos en la tabla venta
    }//fin if() realizar venta
  }

//###################limpiando venta################
  function limpiando_venta(){
    $('#input_importe').val('');
    $("#realizarVenta").modal("hide");
    $("#Total_en_moda_Venta,#cambio_modal_venta").html("");
    $("#totalpagar").html("0.00");
    $("#findProducto").focus();
    limpiartablaventa();
  }

///#############obteniedo productos de la tabla venta
  function get_data_table_venta(){
     var id=[];
      var cantidad=[];
      var unidad=[];
      var descripcion=[];
      var precio=[];
      var subtotal=[];
      var contEach=0;
      var datos={};//objeto para retornar

      $("#tabla_venta tbody").find("tr td:first-child").each(function(){
        if (contEach>0) {
          id.push($(this).html());
          cantidad.push($(this).siblings("td").eq(0).html());
          unidad.push($(this).siblings("td").eq(1).html());
          descripcion.push($(this).siblings("td").eq(2).html());
          precio.push($(this).siblings("td").eq(3).html());
          var sub=$(this).siblings("td").eq(5).html();
          sub=sub.replace("$","");//remplazando el signo de $
          sub=parseFloat(sub);
          subtotal.push(sub.toFixed(2));
        }//fin if
        contEach++;      
      });//fin each
      datos.id=id;
      datos.cantidad=cantidad;
      datos.unidad=unidad;
      datos.descripcion=descripcion;
      datos.precio=precio;
      datos.subtotal=subtotal;
      return datos;
  }
//################FUNCION DE BUSCAR PRODUCTO############################
  function buscarPro(){
    limpiartablabusqueda();
        $("#findProducto").blur();
        $('#ModalBusqueda').modal('show');
        // $("#start0").focus();
        iniciarFocus("start0");
        var producto=$("#findProducto").val();
        $.ajax({
          url:'{{route("buscarProducto")}}',
          type:'POST',
          dataType:'json',
          data:{
            busqueda:producto
          },
          
            }).done(function(e){
              if (e=="") {
               $("#tabla_busqueda tbody").append("<tr class='text-danger'> <td  colspan='8'><b>No se encontró registro!!</b></td></tr>");
              }else{
                for(var x=0;x<e.length;x++){
                $("#tabla_busqueda tbody").append("<tr tabindex='0' class='move'><td >"+e[x].id_p+"</td><td>"+e[x].clave+"</td><td>"+e[x].unidad+"</td><td>"+e[x].descripcion_articulo+"</td><td>"+e[x].precio_venta+"</td><td>"+e[x].existencia+"</td> <td>"+e[x].ubicacion_producto+"</td></tr>");
               }//fin for
              }
              
              });//fin done 
            $("#findProducto").val("");
  }
///########################buscar por codigo de barras###############
  function buscar_x_codigo(codigo){
    var areglo;
    $.ajax({
        url:"{{route('buscarXcodigo')}}",
        type:"POST",
        async:false,
        dataType:'json',
        data:{
          busqueda:codigo
        },success:function(e){
          areglo=e;
        },
        error:function(){
          areglo="";
        }
      });
    return areglo;
  }


</script>
@endsection
