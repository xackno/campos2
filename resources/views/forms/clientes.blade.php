@extends('layouts.app')
@section('content')
<div class="container" style="padding-left: 3px;padding-right: 3px">
	<ol class="breadcrumb" style="padding: 0px">
    <li class="breadcrumb-item active"><a href="./">Inicio</a></li>
    <li class="breadcrumb-item " aria-current="page">Clientes</li>
  </ol>

	<div id="div_alert"></div>
		@if(session('success'))
	<div class="alert alert-success alert-dismissible fade show">
		<h3>{{session('success')}}</h3>
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		    <span aria-hidden="true">&times;</span>
		</button>
	</div>
		@endif
		@if(session('error'))
	<div class="alert alert-danger alert-dismissible fade show">
		<h3>{{session('error')}}</h3>
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		    <span aria-hidden="true">&times;</span>
		</button>
	</div>
	@endif

	<div class="row justify-content-center">
	    <div class="col-md-12">
		    <div class="card">
		       	<div class="card-header " style="background: #2ECC71" >
		       			<span class="text-white" style="font-size: 160%" id="title_card"><b>Clientes <i class="fa fa-users"></i></b></span>	

		       			<a class="btn btn-sm btn-warning float-right" style="margin-left: 5px" href="{{route('rutas')}}"><i class="fa fa-plus"></i> <i class="	fas fa-route"></i> Ruta</a>
		       			<button class="btn btn-sm btn-primary float-right" id="agregarCliente"><i class="fa fa-plus"></i> Cliente</button>
		        </div>
	        	<div class="card-body">

	        		<button class="btn btn-info" id="btn_ver_rutas"><i class="fa fa-eye"></i>Ver rutas</button>
	        		<button class="btn btn-info" id="btn_ver_clientes" style="display: none"><i class="fa fa-eye"></i>Ver clientes</button>

	        		@if(Request::path()=="clientes-search")
		        				<a class="btn btn-info float-right" href="{{route('clientes.index')}}">
		        					<i class="	fas fa-eye "></i> ver todo</a>
		        			@endif


		        			<br><br>
	              	<div class="table-responsive" id="view_rutas" style="display: none" >
					  <table class="table table-striped table-bordered" id="tabla_rutas">
					    	<thead class="table-dark ">
				                  <tr>   
								       <th>ID</th>
								       <th>Nombre</th>
								       <th>Longitud (m)</th>
								       <th>Tiempo de recorrido</th>
								       <th>Origen</th>
								       <th>Destino</th>
								       <th></th>
								       <th><i class="fa fa-cog"></i></th>
								       <th></th>		       
				                  </tr>
				                </thead>
				                <tbody>
				                	@foreach($rutas as $ruta)
				                	<tr scope="row">
					                	<td>{{$ruta->id}}</td>
					                	<td>{{$ruta->nombre_ruta}}</td>
					                	<td>{{$ruta->longitud_ruta}}</td>
					                	<td>{{$ruta->tiempo_recorrido_total}}</td>
						              	<td>{{$ruta->origen}}</td>
						              	<td>{{$ruta->destino}}</td>
						              	<td>
						              		<form action="{{route('see_ruta')}}" method="post" >
						              			@csrf
						              			<input type="number" class="d-none" name="id" value="{{$ruta->id}}">
						              			<button class="btn btn-info btn-sm"><i class="fa fa-eye"></i></button>
						              		</form>
						              		
						              		
						              	</td>
						              	<td>
						              		<form action="{{route('edit_ruta')}}" method="post" >
						              			@csrf
						              			<input type="number" class="d-none" name="id" value="{{$ruta->id}}">
						              			<button class="btn btn-success btn-sm"><i class="fa fa-edit"></i></button>
						              		</form>
						              	</td>
						              	<td>@if($ruta->id!=1)
						              		<button class="btn btn-danger btn-sm" onclick="
						              		var confm=confirm('Desea eliminar esta ruta?');
					              			if(confm==true){
					              			$('#Form_eliminar_ruta{{$ruta->id}}').submit();}
						              		"><i class="fa fa-trash"></i></button>

						              		<form action="{{route('destroy_ruta')}}" method="post" id="Form_eliminar_ruta{{$ruta->id}}" class="d-none">
						              			@csrf
						              			<input type="number" class="d-none" name="id" value="{{$ruta->id}}">
						              		</form>
						              		@endif
						              	</td>
					              	</tr>
					              	@endforeach

				                </tbody>
					  </table>
					  {!!$rutas->render() !!}
					</div>


	              	<!-- ########################################################################################################################################################################################################################################### -->
	              	
	              		<div class="table-responsive" id="view_clientes" >
					  <table class="table table-striped table-bordered" id="tabla_clientes">
					    	<thead class="table-dark ">
				                  <tr>   
					                   <th class="text-center"><i class="fas fa-image text-warning"></i></th>
								       <th>Nombre</th>
								       <!-- <th>Apellidos</th> -->
								       <!-- <th>Municipio</th> -->
								       <th>Localidad</th>
								       <th>Calle</th>
								       <th class="text-center"><i class="fas fa-phone-square text-danger"></i></th>
								       <th class="text-center"><i class="fas fa-mail-bulk text-primary"></i></th>
								       <th>Usuario</th>
								       <th>Status</th>
								       <th>nivel</th>
								       <th><i class="fas fa-money-check-alt text-success"></i></th>
								       <th><i class="fas fa-barcode text-primary"></i></th>
								       <th><i class="fa fa-star text-warning"></i></th>
								       <th><i class="fas fa-route text-success"></i></th>
								       <th style="width: 10%"><i class="fas fa-cogs"></i></th>
				                  </tr>
				                </thead>
				                <tbody>
				                	@foreach($clientes as $cliente)
				                	<tr scope="row">
					                	<td><img src="public/img/avatar/{{$cliente->foto}}" class="imgRedonda"></td>
						              	<td class="text-primary"><b>{{$cliente->nombre}}</b>  {{$cliente->apellidos}}</td>
						              	
						              	<!-- <td>{{$cliente->municipio}}</td> -->
						              	<td class="text-info">{{$cliente->localidad}}<p class="text-primary">{{$cliente->municipio}}</p></td>
						              	<td>{{$cliente->calle}}</td>
						              	<td>{{$cliente->telefono}}</td>
						              	<td class="minimizar text-info">{{$cliente->correo}}</td>
						              	<td>{{$cliente->usuarioApp}}</td>
						              	<td>
						              		@if($cliente->status=="enable")
						              		<p class="text-primary">{{$cliente->status}}</p>
						              		@else
						              		<p class="text-secondary">{{$cliente->status}}</p>
						              		@endif
						              	</td>
						              	<td>{{$cliente->nivel}}</td>
						              	<td><b>${{$cliente->monedero}}</b></td>
						              	<td>{{$cliente->identificacion}}</td>
						              	<td>
						              		<span id="span_star" class="float-right">
						            			@for($x=0;$x<5;$x++)
						            				@if($x<$cliente->calificacion)
						            					<i class="fa fa-star text-warning"></i>
						            				
						            				@endif
						            			@endfor
					                    	</span>
						              	</td>
						              	<td>{{$cliente->nombre_ruta}}</td>
						              	<td class="td_imgs" >
						              		<button class="btn btn-sm btn-danger" 
					              			onclick="
					              			var confm=confirm('Desea eliminar el cliente?');
					              			if(confm==true){
					              			$('#Form_eliminar_cliente{{$cliente->id}}').submit();}">	
					              			
					              			<i class="fa fa-trash" ></i>
					              		</button>

					              		<button class="btn btn-sm btn-success" 
					              		onclick="$('#form_editar_cliente{{$cliente->id}}').submit();">
					              			<i class="fas fa-edit" style="font-size: 80%"></i>
					              		</button>

						              		<form action="{{ route('clientes.destroy',$cliente->id) }}" method="post" class="btnAction d-none" id="Form_eliminar_cliente{{$cliente->id}}">
						              			 @csrf
	                 							 @method('DELETE')
						              			<button class="btn btn-sm btn-danger" ><i class="fa fa-trash"></i></button>
						              		</form>

						              		<form action="{{route('vercliente') }}" method="post" class="btnAction d-none" id="form_editar_cliente{{$cliente->id}}">
						              			 @csrf
	                 							 <input type="hidden" name="id" value="{{$cliente->id}}">
						              			<button class="btn btn-sm btn-success " ><i class="fas fa-edit"></i></button>
						              		</form>
						              	</td>
					              	</tr>
					              	@endforeach

				                </tbody>
					  </table>
					  {!!$clientes->render() !!}
					</div>
						<style type="text/css">
							.minimizar{font-size: 70%}
							.minimizar:hover{font-size: 100%}
							#tabla_clientes tbody tr td,#tabla_rutas tbody tr td{ 
								padding: 5px;margin:0px;
								border: 0.2px solid #eee;
								border-bottom: 2px solid #66bb6a;
								 }
							#tabla_clientes thead tr th,#tabla_rutas thead tr th{ 
								padding:4px;
								padding-left: 3px;
								padding-right: 3px }

							.btnAction{width: 48%;display: inline-block;}
						</style>


	              	</div>
	        	</div>

		    </div> <!-- fin card -->
	    </div><!-- fin col-md-12 -->
	</div><!-- fin content-center -->



		<style type="text/css">
			.imgRedonda {
		    width:40px;
		    height:42px;
		    border-radius:140px;
		    border:2px solid #666;
		}
		.imgPerfil{
			width:60px;
		    height:62px;
		    border-radius:140px;
		    border:2px solid #666;
		}
		</style>




<!--window modal ######modal clientes################-->
  <div class="modal fullscreen-modal fade" id="modal_clientes" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content" >
      	<div class="modal-header text-light" style="background-color: #28a745;" >
      		<h1><i class="fa fa-plus"></i> Cliente <i class="app-menu__icon fa fa-user"></i></h1>
      		<span id="imgParaPerfil"></span>
      	</div>
        <div class="modal-body">
        	<div id="div_alert2"></div>
        	<form action="{{route('clientes.store')}}" method="POST">
        		@csrf
        		<div class="row">

				    <div class="col-xl-4 col-md-6 mb-4">
						<label for="nombre">Nombre</label>
		        		<input type="text" name="nombre" class="form-control" placeholder="Nombre o nombres ">
		        		<label for="apellidos">Apellidos</label>
		        		<input type="text" name="apellidos" class="form-control" placeholder="apellidos">
						<label for="municipio">Municipio</label>
						<input type="text" name="municipio" class="form-control" placeholder="municipio o C.P.">
		        		<label for="localidad">Localidad</label>
		        		<input type="text" name="localidad" class="form-control" placeholder="Localidad">
						
				    </div>
				    <div class="col-xl-4 col-md-6 mb-4">
				    	<label for="calle">Calle</label>
		        		<input type="text" name="calle" class="form-control" placeholder="Nombre y número">

		        		<label for="telefono">teléfono</label>
		        		<input type="tel" name="telefono" class="form-control" >

		        		<label for="correo">Correo</label>
		        		<input type="email" name="correo" class="form-control">

		        		<label for="foto">Foto</label>
		        		<input type="text" name="foto" id="foto" class="form-control" readonly="" value="no-images.jpg">
				    </div>

				    <div class="col-xl-4 col-md-6 mb-4">

						<label for="usuarioApp">Usuario Para la App</label>
		        		<input type="text" name="usuarioApp" class="form-control">

						<label for="password">Contraseña</label>
		        		<input type="password" name="password"  class="form-control">
		        		<label>Tipo de cliente</label>
		        		@auth
                      		@if(Auth::user()->type=='superadmin' || Auth::user()->type=='admin')
		        			<select class="form-control" name="nivel">
		        			<option value="comun">común</option>
		        			<option value="Mayoreo1">Mayoreo 1</option>
		        			<option value="Mayoreo2">Mayoreo 2</option>
		        			<option value="Mayoreo3">Mayoreo 3</option>
		        			<option value="Mayoreo4">Mayoreo 4</option>
		        		</select>
		        		@else
		        			<select class="form-control" name="nivel">
		        			<option value="comun">básico</option>
		        		</select>
		        		@endif
                    	@endauth
                    	<label class="">Número de identificación</label>
                    	<input type="number" name="identificacion" class="form-control">

                    	<input type="number" name="calificacion" class="d-none" value="1" readonly="">
                    	<label>Rutas</label>
                    	<select name="id_ruta" class="form-control" required="">
                    		<option selected="" disabled="">Seleccione una opción</option>
                    		@foreach($rutas as $ruta)
                    		<option value="{{$ruta->id}}">{{$ruta->nombre_ruta}}</option>
                    		@endforeach
                    	</select>
		        		

				 </div>

			</div>
        		
		       	<br><br>
	        	<button type="submit" class="btn btn-success" style="float: right;">Guardar</button>
        	</form>

        	<form enctype="multipart/form-data" id="formsubirFoto" method="post">
        		@csrf
        		<label for="imagen"><b>Subir foto:</b></label>
			  <input type="file" name="imagen" id="imagen" />
			  <button  type="submit" class="btn btn-primary" id="subirImg">subir</button>
        	</form>
		  


        </div>
      </div>
    </div>
  </div>






@endsection

@section('script')
<script type="text/javascript">

//##########ocultar tabla de rutas#########################

	$("#btn_ver_rutas").click(function(){
		$("#title_card").html('<b>Rutas <i class="fa fa-route"></i></b>');
		$("#view_clientes,#btn_ver_rutas").hide();
		$("#view_rutas,#btn_ver_clientes").show();
	});
	$("#btn_ver_clientes").click(function(){
		$("#title_card").html('<b>Clientes <i class="fa fa-users"></i></b>');
		$("#view_clientes,#btn_ver_rutas").show();
		$("#view_rutas,#btn_ver_clientes").hide();
	});

//#############################################################
	$(".btn_toggle").trigger("click");
	


//##########################################################
//###########################################################
if ($(window).width() <= 360) {
	    $(".btn_toggle").trigger("click");
	}
/////-----------------------para subir foto
	$("#formsubirFoto").on("submit", function(e){
	            e.preventDefault();
	            var f = $(this);
	            var formData = new FormData(document.getElementById("formsubirFoto"));
	            formData.append("dato", "valor");
	            //formData.append(f.attr("name"), $(this)[0].files[0]);
	            $.ajax({
	                url: "{{url('/uploadImgUser')}}",
	                type: "post",
	                dataType: "",
	                data: formData,
	                cache: false,
	                contentType: false,
		     processData: false
	            }).done(function(e){
					// alert(e.nombre);
					if (e.status=="success") {

						$("#foto").val(e.nombre);
						$("#imgParaPerfil").html('<img src="public/img/avatar/'+e.nombre+'" class="imgPerfil">');
						$("#div_alert2").html("<div class='alert alert-success' role='alert'>IMAGEN Cargado correctamente.</div>");
						setTimeout(function(){
				        $( "#div_alert2").html('');
				        }, 3500);
					}else{
						$("#div_alert2").html("<div class='alert alert-danger' role='alert'>Error al cargar.</div>");
						setTimeout(function(){
				        $( "#div_alert2").html('');
				        }, 3500);
					}
		}).fail(function (jqXHR, exception) {
	                // Our error logic here
	                console.log(exception);
	            });
	        });
//------------------------------------------------------------------------------------------------------






//------------- minimizar menu------------------------------------
	$("#btn_toggle").trigger("click");
//-------------------------------------------------------

$("#agregarCliente").click(function(){
	$("#modal_clientes").modal('show');
});
</script>
@endsection