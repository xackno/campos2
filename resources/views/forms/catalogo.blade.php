@extends('layouts.layout2')
@section('content')
        <div class="card" >
        	<div class="card-header bg-warning text-white">
        		<h4 class="text-dark float-left" style="text-transform: uppercase;"><i class="fa fa-book text-dark "></i>
            Catálogo carnes del campo </h4> 
            @auth 
            <a href="{{route('home')}}" class="btn btn-outline-success float-right" ><i class="fas fa-home"></i> inicio</a>
            @endauth
            @if (Auth::guest())
            <a href="{{route('login')}}" class="btn btn-outline-success float-right" ><i class="fas fa-user"></i> entrar</a>
            @endif
        	</div>

          <div class="card-body">
   					<div class="row ">
   						@foreach($productos as $producto)

   							<div class="col-xl-4 col-md-6 mb-4">
		   						<div class="card h-100 text-dark border-success mb-3" style="max-width: 18rem;">
						    	<h4 class="text-info">{{$producto->unidad}}</h4>
						    	<div style="background: red;width: 150px;height:150px;margin-left: auto;margin-right: auto; ">
                    @if($producto->fotos=='ninguno')
                    <img class="card-img-top" src="img/productos/no-images.jpg" alt="No hay imagen" style="height: 100%;width: 100%" >
                    @else
                    <?php $fot=explode(',', $producto->fotos); ?>
                    <img class="card-img-top" src="img/productos/{{$fot[0]}}" alt="Card image cap" style="height: 100%;width: 100%" >
                    @endif
						    		
						    	</div>
						    	<div class="card-body" style="margin-top: -20px">
						      		<h5 class="card-title text-danger ">{{$producto->descripcion_articulo}}</h5>
						      		<p class="card-text" style="margin-bottom: 0px">
                        <select class="form-control" style="margin-top: -10px">
                          @auth
                          @if(Auth::user()->type=='administrador')
                              <option value="{{$producto->precio_compra}}" class="text-primary">Compra - ${{number_format($producto->precio_compra,2,'.',',')}}</option>
                              <option value="{{$producto->precio_venta}}" class="text-primary">Venta - ${{number_format($producto->precio_venta,2,'.',',')}}</option>
                            <option value="{{$producto->mayoreo_1}}" class="text-primary">Mayoreo 1 - ${{number_format($producto->mayoreo_1,2,'.',',')}}</option>
                            <option value="{{$producto->mayoreo_2}}" class="text-primary">Mayoreo 2 - ${{number_format($producto->mayoreo_2,2,'.',',')}}</option>
                            <option value="{{$producto->mayoreo_3}}" class="text-primary">Mayoreo 3 - ${{number_format($producto->mayoreo_3,2,'.',',')}}</option>
                            <option value="{{$producto->mayoreo_4}}" class="text-primary">Mayoreo 4 - ${{number_format($producto->mayoreo_4,2,'.',',')}}</option>
                             @endif
                            @endauth
                            <option value="{{$producto->precio_venta}}" class="text-primary">Venta - ${{number_format($producto->precio_venta,2,'.',',')}}</option>
                            <option value="{{$producto->mayoreo_1}}" class="text-primary">Mayoreo 1 - ${{number_format($producto->mayoreo_1,2,'.',',')}}</option>
                            <option value="{{$producto->mayoreo_2}}" class="text-primary">Mayoreo 2 - ${{number_format($producto->mayoreo_2,2,'.',',')}}</option>
                            <option value="{{$producto->mayoreo_3}}" class="text-primary">Mayoreo 3 - ${{number_format($producto->mayoreo_3,2,'.',',')}}</option>
                            <option value="{{$producto->mayoreo_4}}" class="text-primary">Mayoreo 4 - ${{number_format($producto->mayoreo_4,2,'.',',')}}</option>
                        </select>
						      		</p>
                      <span class="text-info">Descripción:</span>
                      <p class="text-dark" style="font-size: 80%;text-align: justify;">
                        {{$producto->descripcion_catalogo}}
                      </p>
						    	</div>
						    		<div class="card-footer border-primary bg-success">
						      			<span class="text-light">Existencia <span class="text-dark">{{$producto->existencia." ".$producto->unidad}}</span></span>
						      			<span class="float-right "><i class="fas fa-2x fa-cart-plus icon_plus_oferta " style="color:#FAD7A0;"></i></span>
						    		</div>
						  		</div>
		   					</div>
   					 
				  @endforeach
   					</div>

   			<div style="width: 80%;overflow-x: scroll;" >
   						{!!$productos->render() !!}
   			</div>
			<br>
		</div>
  </div> <!-- fin card -->




<div class="modal" tabindex="-1" id="alert_para_card" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Retail Stehs</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Descarga Nuestra app desde Google play para poder realizar Pedidos.</p>
        <img src="img/system/googleplay.png" style="width: 80px">
        <img src="img/system/logo-retail.png" style="width: 80px">
        <span class="text-primary" style="font-size: 200%">RetailApp</span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" data-dismiss="modal">Enterado</button>
      </div>
    </div>
  </div>
</div>







@endsection

@section('script')
<script type="text/javascript">
	// $(".btn_toggle").trigger("click");
  if ($(window).width() <= 360) {
       $(".row").css('display','block');
     }if($(window).width() >= 360){
       $(".row").css('display','flex');
     }
	
	$(".icon_plus_oferta").click(function(){
		// alert("sdfds");
		$("#alert_para_card").modal('show');		
	});
</script>
@endsection