@extends('layouts.app')
@section('content')
<div class="container" style="padding-left: -5px;padding-right: -5px;margin:0px;max-width: 100%;">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card" >
            	<div class="card-header" style="background-color:#9B59B6">
            		<h4 class="text-light"><i class="fa fa-book text-dark "></i> Pedidos 
            			<a class="btn btn-warning float-right" href="{{route('paraPedidos')}}"><i class="fa fa-plus"></i></a>
            			<a href="" class="btn btn-success float-right disabled">Pagos</a>
            			<a href="" class="btn btn-primary float-right disabled">Devoluciones</a>
            		</h4>
            	</div>
            	<div class="card-body">
         		<div id="contedido_historial">

         			<div class="table-responsive">
         				<h3 class="text-success"><i class="fa fa-list-alt"></i> Lista de pedidos</h3>

					  <table class="table table-striped table-bordered" id="table_pedidos" >
					    <thead class="table-dark" >
					    	<tr>
					    		<th>#</th>
					    		<th>cliente</th>
					    		<th>fechas</th>
					    		<!-- <th>fecha de enviado</th>
					    		<th>fecha de entregado</th> -->
					    		<!-- <th>ubicación de cliente</th> -->
					    		<th>ubicación de producto</th>
					    		<th>importe</th>
					    		<th>envio</th>
					    		<th>Subtotal</th>
					    		<th>descuento<i class="text-danger fas fa-money-check-alt"></i></th>
					    		<th>total</th>
					    		<th class="text-warning">status</th>
					    		<th><i class="fa fa-cog"></i></th>
					    	</tr>
					    </thead>
					    <tbody id="td_pedidos">
					    	@foreach($pedidos as $pedido)
					    	<tr>
					    		<td>{{$pedido->id}}</td>
					    		<td>{{$pedido->nombre}}</td>
					    		<td >
					    		<p data-toggle="tooltip" data-html="true" title="
					    		<em class='text-primary'>Pedido: </em><b>{{$pedido->fecha_pedido}}</b> 

					    		<em class='text-success'>Envio: </em><b>{{$pedido->fecha_enviado}}</b> 

					    		<em class='text-warning'>Recibido: </em><b>{{$pedido->fecha_recibido}}</b>">
					    				<b>Ver fechas</b>
					    			</p>
					    			
					    		</td>
					    		<td>{{$pedido->ubicacion_actual}}</td>
					    		<td>${{$pedido->importe}}</td>
					    		<td>${{$pedido->costo_envio}}</td>
					    		<td>${{$pedido->subtotal}}</td>
					    		<td class="text-danger">${{$pedido->descuento}}</td>
					    		<td class="text-primary">${{$pedido->total}}</td>
					    		<td class="text-white bg-warning">{{$pedido->status}}</td>
					    		<td>
					    			<button class="btn btn-sm btn-success" 
					              		onclick="$('#edit{{$pedido->id}}').submit();">
					              			<i class="fas fa-edit" style="font-size: 80%"></i>
					              	</button>
					              	@auth
          							@if(Auth::user()->type=="superadmin")
					              	<button class="btn btn-sm btn-danger" 
					              			onclick="$('#Form_eliminar_pedido{{$pedido->id}}').submit();">
					              			<i class="fa fa-trash" ></i>
					              		</button>

					              	<form action="{{ route('pedidoss.destroy',$pedido->id) }}" method="post" class="btnAction d-none" id="Form_eliminar_pedido{{$pedido->id}}">
						              @csrf
	                 				@method('DELETE')
						             <button class="btn btn-sm btn-danger" ><i class="fa fa-trash"></i></button>
						             </form>
						             @endif
						             @endauth
					              	<form action="{{route('verPedido') }}" method="post" class="btnAction d-none" id="edit{{$pedido->id}}">
					              			@csrf
                 						<input type="hidden" name="id" id="id_editar" value="{{$pedido->id}}">
					              		<button class="btn btn-sm btn-success">
					              		</button>
					              	</form>
					    		</td>
					    	</tr>
					    	@endforeach
					    	
					    </tbody>
					  </table>
					  {!!$pedidos->render() !!}
					  <style type="text/css">
					  	
					  	.fechas{margin: 0px;}
					  	#tab_listventa thead tr th{
					  		padding:2px;
					  		padding-right: 3px;
					  		padding-left: 3px;
					  		border: 1px solid #eee;
					  		}
					  		#tab_listventa tbody tr td{
					  		padding:0px;
					  		padding-right: 3px;
					  		padding-left: 3px;
					  		border: 1px solid #eee;
					  		border-bottom: 2px solid #aaa;
					  	}
					  </style>
					</div>

         		</div><!-- fin div contenido_historial -->

           		</div><!-- fn card -->
        </div> 
            <!-- fin card -->
        </div>
    </div>
	</div>


<div class="modal" tabindex="-1" id="modal_agregar_pedido" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header bg-info text-white">
        <h5 class="modal-title id_modal"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
  
   
      </div>

    </div>
  </div>
</div>





@endsection

@section('script')
<script type="text/javascript">
	$('[data-toggle="tooltip"]').tooltip();
	$(".btn_toggle").trigger("click");
	if ($(window).width() <= 360) {
	    $(".btn_toggle").trigger("click");
	}

</script>
@endsection