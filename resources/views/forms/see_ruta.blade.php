@extends('layouts.app')
@section('content')
<div class="container" style="padding-left: 3px;padding-right: 3px">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card" >
            	<div class="card-header text-white" style="background:#28a745">
            		 <h4><i class="fas fa-route"></i> Rutas {{$ruta->nombre_ruta}}
            		 	<span class="float-right">ID {{$ruta->id}}</span>
            		 </h4>
            	</div>
            	<div class="card-body">
            		
            		
            		<div style="width: 100%" id="msj"></div>
            		
            <div id="map" style="width: 100%;height: 500px;background:#afecde">
			</div>

            	</div> <!-- fin body card -->
           	</div>
        </div> 
    </div>
</div>

@endsection

@section('script')

<script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyCjXtZCg-4A85t_1Nvxyp3ww0edOp7lqQ8"></script>

<script type="text/javascript" src="public/js/gmaps.js"></script>
<script type="text/javascript">
//$#################################################################
	if ($(window).width() <= 360) {
		    $(".btn_toggle").trigger("click");
		}
		$(".btn_toggle").trigger("click");
//################extrado datos de orgen y destino################
	<?php
		$origen=$ruta->origen;
		$origen=explode(",",$origen);
		$destino=$ruta->destino;
		$destino=explode(",", $destino);
	  ?>


//###################dibujando mapa#####################################
	dibuando_mapa();
	function dibuando_mapa(){
	var map;
	var latOrigin={{$origen[0]}};
	var lngOrigin={{$origen[1]}};
	var latDestino={{$destino[0]}};
	var lngDestino={{$destino[1]}};
      map = new GMaps({
        el: '#map',
        lat:latOrigin,
        lng:latOrigin
      });
      map.setCenter(latOrigin, lngOrigin);

      map.addMarker({ lat: latOrigin,
       lng: lngOrigin,
       title: 'SUCURSAL CENTRAL'
   });

	<?php 
		$latitud_seccion=$ruta->latitud_seccion;
		$latitud_seccion=explode("%", $latitud_seccion);
		$cantSeccion=count($latitud_seccion);

	
		for($x=1;$x<$cantSeccion;$x++){
			$Newposition=explode(",", $latitud_seccion[$x]);
			$nombre_secciones=explode("%",$ruta->nombre_secciones);
			$lastLat=0;$lastLng=0;
			if ($x>1) {
				$lastposition=explode(",", $latitud_seccion[$x-1]);
				$lastLat=$lastposition[0];
				$lastLng=$lastposition[1];
			}
		
		$newLat=$Newposition[0];
		$newLng=$Newposition[1];
		

		?>
		var newlat={{$newLat}};
		var newlng={{$newLng}};
		var lastlat={{$lastLat}};
		var lastlng={{$lastLng}};
		if ({{$x}}==1) {
			map.drawRoute({
          origin: [latOrigin, lngOrigin],  
          destination: [newlat,newlng],
          travelMode: 'driving',
          strokeColor: '#000000',
          strokeOpacity: 0.6,
          strokeWeight: 5
        });
       map.addMarker({
       	lat: newlat,
        lng: newlng,
        title:'{{$nombre_secciones[$x]}}'
       }); 
   }if({{$x}}>1){
   	map.drawRoute({
          origin: [lastlat,lastlng],  
          destination: [newlat,newlng],
          travelMode: 'driving',
          strokeColor: '#000000',
          strokeOpacity: 0.6,
          strokeWeight: 5
        });
       map.addMarker({
       	lat: newlat,
        lng: newlng,
       	title:'{{$nombre_secciones[$x]}}'
       }); 
   }//fin if script	
	<?php
	}//fin for de php	
?>
}//Fin function
    

</script>

@endsection