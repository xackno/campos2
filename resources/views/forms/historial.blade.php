@extends('layouts.app')
@section('content')
<div class="container" style="padding-left: 3px;padding-right: 3px">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card" >
            	<div class="card-header" style="background-color:#FF7051">
            		<h4 class="text-light"><i class="fa fa-book text-dark "></i> Historial</h4>
            	</div>
            	<div class="card-body">
            		<p class="row botones_opciones">
            			<button class="btn btn-info col-xl-3 col-md-6 mb-4">ventas</button>
            			<!-- <button class="btn btn-success col-xl-3 col-md-6 mb-4">Productos vendidos</button>
            			<button class="btn btn-warning col-xl-3 col-md-6 mb-4">Pedidos</button>
            			<button class="btn btn-primary col-xl-3 col-md-6 mb-4">Productos pedidos</button> -->
            			<style type="text/css">
            				.botones_opciones button{padding-left: 0px}
            			</style>
            		</p>
         		<div id="contedido_historial">

         			<div class="table-responsive">
         				<h3 class="text-primary"><i class="fa fa-list-alt"></i> Lista de ventas</h3>
					  <table class="table table-striped table-bordered" id="tab_listventa" >
					    <thead class="table-dark" >
					    	<tr>
					    		<th>id</th>
					    		<th>Atendio</th>
					    		<th>Turno</th>
					    		<th style="font-size: 70%">Pago con</th>
					    		<th style="font-size: 70%">Total de venta</th>
					    		<th>Tipo</th>
					    		<th>Cliente</th>
					    		<th>Domicilio</th>
					    		<th>fecha</th>
					    		<th><i class="fa fa-cog"></i></th>
					    	</tr>
					    </thead>
					    <tbody id="tb_historial_ventas">
					    	@foreach($ventas as $venta)
					    		<tr>
					    			<td>{{$venta->id}}</td>
					    			<td class="text-info">{{$venta->user}}</td>
					    			<td>{{$venta->id_turno}}</td>
					    			<td>${{$venta->efectivo}}</td>
					    			<td class="text-primary">${{$venta->total_venta}}</td>
					    			<td class="text-success">{{$venta->tipo}}</td>
					    			<td>{{$venta->nombre}}</td>
					    			<td>{{$venta->domicilio}}</td>
					    			<td style="font-size: 70%">{{$venta->created_at}}</td>
					    			<td onclick="verVenta({{$venta->id}});"><i class="fa fa-eye text-primary"></i></td>
					    		</tr>
					    	@endforeach
					    </tbody>
					  </table>
					  <style type="text/css">
					  	#tab_listventa thead tr th{
					  		padding:2px;
					  		padding-right: 3px;
					  		padding-left: 3px;
					  		border: 1px solid #eee;
					  		}
					  		#tab_listventa tbody tr td{
					  		padding:0px;
					  		padding-right: 3px;
					  		padding-left: 3px;
					  		border: 1px solid #eee;
					  		border-bottom: 2px solid #aaa;
					  	}
					  </style>
					</div>

         		</div><!-- fin div contenido_historial -->

           		</div><!-- fn card -->
        </div> 
            <!-- fin card -->
        </div>
    </div>
	</div>


<div class="modal" tabindex="-1" id="modal_pro_vendidos" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header bg-info text-white">
        <h5 class="modal-title id_modal"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<p class="text-success">Productos vendidos en esta venta</p>
      		<div class="table-responsive" style="overflow-y: scroll;height: 400px">
			  <table class="table table-striped" id="tab_modal_vendido">
			    <thead class="table-dark">
			    	<tr>
			    		<th class="d-none">id</th>
			    		<th>Cantidad</th>
			    		<th>Unidad</th>
			    		<th>Descripción</th>
			    		<th>Precio</th>
			    		<th>total</th>
			    		<th>Foto</th>
			    	</tr>
			    </thead>
			    <tbody id="tb_modal_vendido">
			  	
			  </tbody>
			  </table>
			</div>
      </div>
      <div class="modal-footer" style="background: #efefef">
      	<button class="btn btn-success">Reimprimir</button>
      </div>

    </div>
  </div>
</div>





@endsection

@section('script')
<script type="text/javascript">
	$(".btn_toggle").trigger("click");
	if ($(window).width() <= 360) {
	    $(".btn_toggle").trigger("click");
	}


var verVenta=function (id) {
	// alert("ver"+id);
	$(".id_modal").html("VENTA "+ id);
	$("#modal_pro_vendidos").modal("show");
	$("#tb_modal_vendido").html('');
	$.ajax({
		url:'{{route("Lista_vendidos")}}',
		type:'post',
		dataType:'json',
		data:{id_venta:id},
		success:function(data){

			for(var x=0;x<data.length;x++){
			// alert(data[x].id);
			var foto=data[x].fotos;
			foto=foto.split(',');
			$("#tb_modal_vendido").append('<tr>  <td class="d-none">'+data[x].id
				+' </td> <td>'+data[x].cantidad
				+'</td> <td>'+data[x].unidad
				+'</td><td>'+data[x].descripcion_articulo
				+'</td><td>$'+(data[x].total)/(data[x].cantidad)
				+'</td> <td>'+data[x].total
				+'</td> <td><img src="public/img/productos/'+foto[0]+'" style="width:50px"></td></tr>');
			}
		}
	});

}


</script>
@endsection