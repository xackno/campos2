@extends('layouts.app')
@section('content')
<div class="container" style="padding-left: 3px;padding-right: 3px">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card" >
            	<div class="card-header" style="background-color:#FF7051">
            		<h4 class="text-light"><i class="fa fa-cog text-dark "></i> Configuración</h4>
            	</div>
            	<div class="card-body">
            		<label>Usuarios del sistema</label>
            		<div class="table-responsive">
            		<table class="table table-striped table-bordered">
            			<thead class="table-dark" >
            				<tr>
            					<th>foto</th>
            					<th>id</th>
            					<th>nombre</th>
            					<th>usuario</th>
            					<th>email</th>
            					<th>tipo</th>
            					<th>local</th>
            					<th><i class="fa fa-cog text-success"></i></th>
            					
            				</tr>
            			</thead>
            			<tbody>
            				@foreach($user as $usuario)
            				<tr>
            					<td><img src="public/img/avatar/{{$usuario->foto}}" style="width: 50px"></td>
            					<td>{{$usuario->id}}</td>
            					<td>{{$usuario->name}}</td>
            					<td>{{$usuario->user}}</td>
            					<td>{{$usuario->email}}</td>
            					<td>{{$usuario->type}}</td>
            					<td>{{$usuario->nombre}}</td>
            					<td>
            						<button class="btn btn-success" onclick="edit_user({{$usuario->id}},'{{$usuario->name}}','{{$usuario->user}}','{{$usuario->email}}','{{$usuario->type}}','{{$usuario->nombre}}','{{$usuario->foto}}','{{$usuario->password}}');"><i class="fas fa-edit"></i></button>
            						
            					</td>
            				</tr>
            				@endforeach
            			</tbody>
            		</table>
            	</div>
            	
           		</div><!-- fn card -->
        </div> 
            <!-- fin card -->
        </div>
    </div>
	</div>






<!--window modal ######modal clientes################-->
  <div class="modal fullscreen-modal fade" id="modal_users" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content" >
      	<div class="modal-header text-light" style="background-color: #28a745;" >
      		<h1 class="text-white"><i class="app-menu__icon fa fa-user"></i> Usuarios del sistema <span id="idspan"></span></h1>
      		<span id="imgParaPerfil"></span>
      	</div>
        <div class="modal-body">
        	<div id="div_alert2"></div>
        	<form id="for_update_user">
        		<div class="row">
				    <div class="col-xl-4 col-md-6 mb-4">
				    	<input type="number" name="id" id="id" class="d-none">
						<label for="name" class="text-primary">Nombre</label>
		        		<input type="text" name="name" id="name" class="form-control" placeholder="Nombre o nombres ">
		        		<label for="user" class="text-info">Usuario</label>
		        		<input type="text" name="user" id="user" class="form-control" placeholder="Usuario">
						<label for="email" class="text-danger">Email</label>
						<input type="email" name="email" id="email" class="form-control" placeholder="Correo">
		        		
						
				    </div>
				    <div class="col-xl-4 col-md-6 mb-4">
				    	<label for="password" class="text-success">Contraseña</label>
		        		<input type="password" name="password" id="password" class="form-control" placeholder="Contraseña">
		        		<input type="text" name="password_old" id="password_old" class="form-control d-none" >

				    	<label class="text-warning">Tipo</label>
				    	<select name="type" class="form-control" id="type">
				    		 @if (Route::has('login'))
			                    @auth
			                      @if(Auth::user()->type=='superadmin' )
			                      	<option value="superadmin">Superadmin</option>
			                      	<option value="admin">admin</option>
			                      	<option value="vendedor">vendedor</option>
			                      	@else
			                      	<option value="vendedor">vendedor</option>
			                      @endif
			                    @endauth
			                    @else
			                    <option disabled="" selected="">Es necesario loguearse</option>
			                  @endif
				    	</select>

				    	<label class="text-primary">Local a la que pertenece</label>
				    	<select class="form-control" name="local" id="local">
				    		<!-- <option selected="" disabled="">Selecciones uno...</option> -->
				    		@foreach($locales as $local)
				    			<option value="{{$local->id}}">{{$local->nombre}}</option>
				    		@endforeach
				    	</select>
				    </div>

				    <div class="col-xl-4 col-md-6 mb-4">
				    	<label>Foto</label>
				    	<input type="text" name="foto" id="foto" class="form-control" readonly="">
				 </div>
			</div>
		</form>

        		
		       	<br><br>
	        	<button  class="btn btn-success" id="btnupdate_user" style="float: right;">Guardar</button>
        	</form>

        	<form enctype="multipart/form-data" id="formsubirFoto" method="post">
        		@csrf
        		<label for="imagen"><b>Subir foto:</b></label>
			  <input type="file" name="imagen" id="imagen" />
			  <button  type="submit" class="btn btn-primary" id="subirImg">subir</button>
        	</form>
		  


        </div>
      </div>
    </div>
  </div>

<style type="text/css">
	.imgPerfil{width: 40px;border-radius:20px}
</style>










@endsection

@section('script')
<script type="text/javascript">
	$(".btn_toggle").trigger("click");
	if ($(window).width() <= 360) {
	    $(".btn_toggle").trigger("click");
	}

$("#btnupdate_user").click(function(){
	$.ajax({
		url:'{{route("updateuser")}}',
		type:'post',
		data:$("#for_update_user").serialize(),
		success:function(e){
			if (e="success") {
				$("#modal_users").modal("hide");
				location.reload();
			}	
		},
		error:function(){
			alert("Hubo un error. porfavor verifique la información de los campos.");
		}
	});

});


var edit_user=function (id,name,user,email,type,local,foto,password) {
	$("#idspan").html(id);
	$("#id").val(id);
	$("#name").val(name);
	$("#user").val(user);
	$("#email").val(email);
	$("#type").val(type);
	// $("#local").val(local);
	$("#foto").val(foto);
	$("#password_old").val(password);
	$("#modal_users").modal("show");
}





/////-----------------------para subir foto
	$("#formsubirFoto").on("submit", function(e){
	            e.preventDefault();
	            var f = $(this);
	            var formData = new FormData(document.getElementById("formsubirFoto"));
	            formData.append("dato", "valor");
	            //formData.append(f.attr("name"), $(this)[0].files[0]);
	            $.ajax({
	                url: "{{url('/uploadImgUser')}}",
	                type: "post",
	                dataType: "",
	                data: formData,
	                cache: false,
	                contentType: false,
		     processData: false
	            }).done(function(e){
					// alert(e.nombre);
					if (e.status=="success") {

						$("#foto").val(e.nombre);
						$("#imgParaPerfil").html('<img src="public/img/avatar/'+e.nombre+'" class="imgPerfil">');
						$("#div_alert2").html("<div class='alert alert-success' role='alert'>IMAGEN Cargado correctamente.</div>");
						setTimeout(function(){
				        $( "#div_alert2").html('');
				        }, 3500);
					}else{
						$("#div_alert2").html("<div class='alert alert-danger' role='alert'>Error al cargar.</div>");
						setTimeout(function(){
				        $( "#div_alert2").html('');
				        }, 3500);
					}
		}).fail(function (jqXHR, exception) {
	                // Our error logic here
	                console.log(exception);
	            });
	        });
//------------------------------------------------------------------------------------------------------




</script>
@endsection