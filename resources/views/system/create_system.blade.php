@extends('layouts.layout2')
@section('content')

<div class="card">
  <div class="card-header " style="background: #28B463">
    <h4 class="text-white">Registrar un nuevo negocio <i class="fas fa-dumpster"></i></h4>
  </div>
  <div class="card-body">
    <div class="alert alert-warning text-dark" id="msj_info">
       <button type="button" class="close" data-dismiss="alert" aria-label="Close">
         <span aria-hidden="true">&times;</span>
       </button>
        <i class="fas fa-exclamation-triangle text-white"></i>
         Esta a punto de crear su negocio en nuestro sistema de RETAIL, es necesario de proporcionar toda su información corretamente.<br>cualquier duda comunicarse al +52 953 176 8022, o al correo info@stehs.com 
      </div>
    <div id="div_alert2"></div>
    
    <div class="progress">
      <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0" id="progress"></div>
    </div>

    <div class="row">
      <div class="col-xl-3 col-md-6 mb-4">
        <h4>Información del negocio</h4>
        <label for="nombre">Nombre</label>
        <input type="text" id="nombre" class="form-control">
        <label>Descripción</label>
        <textarea class="form-control" id="descripcion"></textarea>
        <label>Ubicación <i class=" fas fa-map-marker-alt text-success"></i></label>
        <div class="input-group">
          <input type="text" id="ubicacion" class="form-control" placeholder="Cordenadas" readonly="">
          <button class="btn btn-info btn-sm" id="btn_select_ubicacion">Seleccionar</button>
        </div>

        <label>LOGOTIPO</label>
        <input type="text" name="logo" id="logo" class="form-control" readonly="">

        <form enctype="multipart/form-data" id="cargar_logo_negocio" method="post">
            @csrf
            <div class="input-group">
              <label for="imagen"><b>Subir logotipo:</b></label>
              <input type="file" name="imagen" id="imagen" />
              <button  type="submit" class="btn btn-primary" id="subirImg">Cargar</button>
            </div>
        </form>

      </div>
      <!-- fin primer col -->
      <div class="col-xl-3 col-md-6 mb-4">
        <h4 class="text-primary">Información del responsable</h4>
        <label>Nombre</label>
        <input type="text" id="nombre_responsable" class="form-control">
        <label>Apellidos</label>
        <input type="text" id="apellidos" class="form-control" placeholder="paterno y materno">
        <label>Número de teléfono <i class="fas fa-phone text-info"></i> </label>
        <input type="tel" id="number_phone" class="form-control">
        <label>Email</label>
        <input type="email" id="email" class="form-control" placeholder="ejemplo@servidor.com">
      </div>
      <div class="col-xl-6 col-md-6 mb-4">
          <div id="map" style="width: 100%;height: 500px;background:#afecde;display: none">
          </div>
    </div><br>
  </div>
  <div class="row" style="width: 100%">
      <div class="card-header bg-secondary" style="width: 100%">
          <span class="text-white"><i class="fas fa-table fa-2x"></i>  Diseñar la tabla de productos (precios)</span>
          <a class="btn btn-primary btn-sm" data-toggle="collapse" href="#collapse" role="button" aria-expanded="false" aria-controls="collapseExample">
          <i class="fas fa-chevron-circle-right"></i>
        </a>
      </div>
      <div class="collapse" id="collapse" style="width: 100%">
        <div class="card card-body" >
          <span class="badge badge-danger">Tabla por defecto</span>
          <table class="table table-sm">
            <thead>
              <tr>
                <th>precio_compra</th>
                <th>precio_venta</th>
                <th>mayoreo_1</th>
                <th>mayoreo_2</th>
                <th>mayoreo_3</th>
                <th>mayoreo_4</th>
              </tr>
            </thead>
          </table><br>
          <h5 class="text-success">Modificar campos de los precios en la tabla productos</h5>
          <div class="row">
            <div class="col-xl-6 col-md-6 mb-4">
              <div style="width: 100%" class="input-group">
                <label style="width: 50%"><b>Nombre de Columna</b></label>
                <label><b>Permisos</b></label>
              </div>
              <div class="input-group">
                <input type="text" id="nombre_columna" class="form-control" placeholder="Nombre">
                <select class="form-control" id="select_permisos">
                  <option value="PSA">para solo superadmin</option>
                  <option value="PA">para admin</option>
                  <option value="PV">para vendedor</option>
                </select>
                <button class="btn btn-primary btn-sm" id="agregar_columna">Agregar</button>
              </div>
              
            </div>
            <div class="col-xl-3 col-md-6 mb-4">
            </div>
          </div>
          
          <br>
          <span class="badge badge-success">Nuevo diseño</span>
          <table class="table table-sm" id="nuevo_diseño">
            <thead>
              <tr></tr>
            </thead>
          </table>

        </div>
      </div>
  </div>
  <br><br>
  <div class="row">
      <div class="col-xl-6 col-md-6 mb-4">
        <button class="btn btn-success float-right" id="btn_guardar_system"><i class="fas fa-save"></i> Guardar y crear</button>
        <i class="fas fa-spinner float-right fa-2x fa-spin text-primary"  id="icon_espera"  style="margin-right: 5%;display: none"></i>
        <i class="fas fa-check-circle float-right fa-2x  text-success"  id="iconlisto"  style="margin-right: 5%;display: none"></i>
      </div>
    </div>
</div>




<div class="modal fullscreen-modal fade" id="madal_msj_registro" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content" >
        <div class="modal-header text-light" style="background-color: #28a745;" >
          <h4>Retail</h4>
        </div>
        <div class="modal-body text-center">
                  


        </div>
      </div>
    </div>
  </div>




@endsection

@section('script')

<script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyCjXtZCg-4A85t_1Nvxyp3ww0edOp7lqQ8"></script>

<script type="text/javascript" src="public/js/gmaps.js"></script>

<script type="text/javascript">
var precios_producto="";
$("#agregar_columna").click(function(){

var permiso=$("#select_permisos").val();
var nombreC=$("#nombre_columna").val();
nombreC=nombreC.replace(" ","_");

if (nombreC[0]>=0) {
  alert("El nombre no debe de empezar con un número ni un caracter, y debe de contener solo letras[a-z] y después números de [0-9]");
  return 0;
}
  $("#nuevo_diseño thead tr").append("<th>"+nombreC+"</th>");
  precios_producto=precios_producto+","+nombreC+"/"+permiso;

});

//para el boton de guardar system--------------------------------
  $("#btn_guardar_system").click(function(){
    if (precios_producto=="") {
      precios_producto=",precio_compra/PSA,precio_venta/PV,mayoreo_1/PV,mayoreo_2/PV,mayoreo_3/PV,mayoreo_4/PV";
    }

    //si no hay ningun campo vacio ejecutar el ajax
    if ($("#nombre").val()!='' && $("#descripcion").val()!=''&&
        $("#nombre_responsable").val()!='' && $("#apellidos").val()!=''&&
        $("#number_phone").val()!='' && $("#email").val()!=''
     ) {
      $("#btn_guardar_system").hide(); 
    $("#icon_espera").show();
    $.ajax({
      url:"{{route('guardar_negocio')}}",
      type:"post",
      dataType:"json",
      data:{
        nombre_negocio:$("#nombre").val(),
        descripcion:$("#descripcion").val(),
        ubicacion:$("#ubicacion").val(),
        logotipo:$("#logo").val(),
        nombre_responsable:$("#nombre_responsable").val(),
        apellidos:$("#apellidos").val(),
        number_phone:$("#number_phone").val(),
        email:$("#email").val(),
        precios_producto:precios_producto
      },success:function(e){
       
        if (e.req=="success" && e.clonado=="success") {
          precios_producto="";
          $("#iconlisto").show();
          $("#nombre,#descripcion,#nombre_responsable,#apellidos,#number_phone,#email,#logo").val('');
          
          $("#madal_msj_registro").modal('show');
          $("#madal_msj_registro .modal-body").html("<h1><i class='fas fa-check-circle fa-3x  text-success'></i></h1> <br> Se registro correctamente este negocio asi como su base de datos y esta lista para empezar, utilize como usuario: <b>admin</b> y como contraseña: <b>admin123</b> para poder ingresar, es recomendable cambiar la contraseña después de ingresar. " );


          $("#icon_espera").css("display","none");
        }else{
          $("#btn_guardar_system").show(); 
          $("#icon_espera").hide();
          alert("ERROR, Nombre no disponible para este negocio.");
        }

      },error:function(e){
        $("#btn_guardar_system").show(); 
          $("#icon_espera").hide();
        alert("Error con el servidor, comuniquese con su administrador");
      }
    });
    }else{//si algun campo esta vacio
     alert("Necesita llenar todo los campos");}
  });
//----------for progress bar----------------
    var progress=0;//12.5
    $("#nombre,#descripcion,#nombre_responsable,#apellidos,#number_phone,#email").focusout(function(){
      if ($(this).val()!='') {
        progress=progress+12.5;
        $("#progress").attr("style","width:"+progress+"%");
        $("#progress").html(progress+"%");
      }
      if (progress==100) {
        $("#progress").removeClass("progress-bar-animated");
      }
    });
    $("#nombre,#descripcion,#nombre_responsable,#apellidos,#number_phone,#email").focusin(function(){
      if ($(this).val()!='') {
        progress=progress-12.5;
        $("#progress").attr("style","width:"+progress+"%");
        $("#progress").html(progress+"%");
      }
    });
//-----------ocultando el mensaje inicial----------------
    setInterval(function(){
      $("#msj_info").hide();
    },8000);
//--------------al hacer click en el btn seleccionar ubicacion------
    $("#btn_select_ubicacion").click(function(){
      $("#map").show();
      dibuando_mapa();
    });
//###################dibujando mapa#####################################
  
  function dibuando_mapa(){
  var map;
      map = new GMaps({
        el: '#map',
        lat: 17.265146,
        lng: -97.6823883
      });
      var cont=0;

    map.addListener('click', function(e) {
      map.removeMarkers();
          lat = e.latLng.lat();    
          lng = e.latLng.lng();
    $("#ubicacion").val(lat+","+lng);
    if (cont==0) {
      cont++;
      progress=progress+12.5;
    $("#progress").attr("style","width:"+progress+"%");
    $("#progress").html(progress+"%");
    }

       map.addMarker({lat: lat, lng: lng});  
        });
  }

//_______________cargar logo del negocio---------------------------
  var cont2=0;
  $("#cargar_logo_negocio").on("submit", function(e){
    $("#subirImg").html("Guardar <i class='fas fa-spinner fa-spin text-white'></i>");

    e.preventDefault();
    var f = $(this);
    var formData = new FormData(document.getElementById("cargar_logo_negocio"));
    formData.append("dato", "valor");
    //formData.append(f.attr("name"), $(this)[0].files[0]);
    $.ajax({
        url: "{{url('/uploadlogotipo')}}",
        type: "post",
        dataType: "",
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
      }).done(function(e){
        // alert(e.nombre);


        if (e.status=="success") {
            $("#subirImg").html("Guardar <i class='fas fa-check-circle text-success'></i>");

            $("#logo").val(e.nombre);
            $("#avatarUser").html("<img src='public/img/avatar/"+e.nombre+"' class='flotante' style='width:50px'>");
            
            if (cont2==0) {
              progress=progress+12.5;
              $("#progress").attr("style","width:"+progress+"%");
              $("#progress").html(progress+"%");
              if (progress==100) {
                $("#progress").removeClass("progress-bar-animated");
              }
              cont2++;

            }

            $("#div_alert2").html("<div class='alert alert-success' role='alert'>IMAGEN Cargado correctamente.</div>");
            setTimeout(function(){
            $( "#div_alert2").html('');
            }, 3500);
        }else{
            $("#div_alert2").html("<div class='alert alert-danger' role='alert'>Error al cargar.</div>");
            setTimeout(function(){
            $( "#div_alert2").html('');
            }, 3500);
        }
      }).fail(function (jqXHR, exception) {
        // Our error logic here
        console.log(exception);
    });
  });

</script>

@endsection
