<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>CAMPO 2</title>


  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <link rel="stylesheet" href="../resources/plugins/fontawesome-free/css/all.min.css">
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="../resources/dist/css/adminlte.min.css">
  <link rel="icon" href="{{asset('img/system/default.png')}}" type="image/x-icon"/>
  <meta name="csrf-token" content="{{ csrf_token() }}">
</head>

<body >
  <div class="content-wrapper">
    <div class="content">
      <div class="container-fluid">

        @yield('content')

        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>

<script src="../resources/plugins/jquery/jquery-3.2.1.min.js"></script>
<script src="../resources/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="../resources/dist/js/adminlte.js"></script>
<script type="text/javascript" src="../resources/plugins/datatables/datatables.min.js"></script>
@yield('script')
</body>

<script type="text/javascript">
    $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });


    $('body').addClass('sidebar-collapse');
        $(window).trigger('resize');
</script>

</html>
