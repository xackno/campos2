<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>CAMPO 2</title>


  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <link rel="stylesheet" href="../resources/plugins/fontawesome-free/css/all.min.css">
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="../resources/dist/css/adminlte.min.css">
   <link rel="icon" href="{{asset('img/system/default.png')}}" type="image/x-icon"/>
  <meta name="csrf-token" content="{{ csrf_token() }}">
</head>

<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">

      <li class="nav-item">
        <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button">
          <i class="fas fa-user"></i>
        </a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->














  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary bg-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      <img src="../resources/dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">CARNES DEL CAMPO 2</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">


      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <li class="nav-item">
                <a href="{{route('ventas.index')}}" class="nav-link">
                <i class="nav-icon fas fa-shopping-cart"></i>
                <p class="text-white">VENTAS</p>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ route('productos.index') }}" class="nav-link">
                <i class="nav-icon fas fa-shopping-cart"></i>
                <p class="text-white" >ARTICULOS  <span class="badge badge-info right">2000</span></p>
                </a>
            </li>
            @auth
          @if(Auth::user()->type=="administrador")
            <li class="nav-item">
                <a href="{{route('clientes.index')}}" class="nav-link">
                <i class="nav-icon fas fa-users"></i>
                <p class="text-white" >CLIENTES</p>
                </a>
            </li>
              @endif
          @endauth
          <li class="nav-item">
                <a href="{{route('pedidos')}}" class="nav-link">
                <i class="nav-icon fas fa-users"></i>
                <p class="text-white" >PEDIDOS</p>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{route('historial')}}" class="nav-link">
                <i class="nav-icon fas fa-users"></i>
                <p class="text-white" >HISTORIAL</p>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{route('catalogo')}}" class="nav-link">
                <i class="nav-icon fas fa-users"></i>
                <p class="text-white" >CATÁLOGO</p>
                </a>
            </li>
            @auth
              @if(Auth::user()->type=="administrador")
                <li class="nav-item">
                    <a href="{{route('ofertas.index')}}" class="nav-link">
                    <i class="nav-icon fas fa-users"></i>
                    <p class="text-white" >OFERTAS</p>
                    </a>
                </li>
              @endif
              @endauth








          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-copy"></i>
              <p>
                Layout Options
                <i class="fas fa-angle-left right"></i>
                <span class="badge badge-info right">1</span>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="pages/layout/top-nav.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Top Navigation</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/layout/top-nav-sidebar.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Top Navigation + Sidebar</p>
                </a>
              </li>
              
            </ul>
          </li>
  
          <li class="nav-header">EXAMPLES</li>
          <li class="nav-item">
            <a href="pages/calendar.html" class="nav-link">
              <i class="nav-icon fas fa-calendar-alt"></i>
              <p>
                Calendar
                <span class="badge badge-info right">2</span>
              </p>
            </a>
          </li>


        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <div class="content">
      <div class="container-fluid">

        @yield('content')


















        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark text-center" style="padding: 5px">
      <h4>administrador</h4>
      <hr>

  </aside>

</div>
<script src="../resources/plugins/jquery/jquery-3.2.1.min.js"></script>
<script src="../resources/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="../resources/dist/js/adminlte.js"></script>
<script type="text/javascript" src="../resources/plugins/datatables/datatables.min.js"></script>
@yield('script')
</body>

<script type="text/javascript">
    $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });


    $('body').addClass('sidebar-collapse');
        $(window).trigger('resize');
</script>


<script type="text/javascript">
  
//#################OBTENER FECHA Y HORA#########################
  //funcion obteniendo fecha
  function fecha(){
    var fecha = new Date();
    var dia="";var mes="";
    var meses=['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'];
    if (fecha.getDate()<10) {
      dia="0"+fecha.getDate();
    }else{
      dia=fecha.getDate();
    }

    var fechahoy=(dia+"/"+meses[fecha.getMonth()]+"/"+fecha.getFullYear());
    return fechahoy;
  }

  function hora(){
    var fecha = new Date();
    var h="";var m=""; var s="";
    if (fecha.getHours()<10) {h="0"+fecha.getHours();}else{h=fecha.getHours();}
    if (fecha.getMinutes()<10) {m="0"+fecha.getMinutes();}else{m=fecha.getMinutes();}
    if (fecha.getSeconds()<10) {s="0"+fecha.getSeconds();}else{s=fecha.getSeconds();}
    var horahoy=h+":"+m+":"+s;
    return horahoy;
  }
  $("#fecha").text(fecha());
  setInterval(function(){ $("#hora").text(hora());}, 1000);
//################################################################
</script>

</html>
