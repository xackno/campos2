<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Productos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
           Schema::create('productos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('clave');
            $table->text('descripcion_articulo');
            $table->string('unidad');
            $table->float('precio_compra');
            $table->float('precio_venta');
            $table->float('mayoreo_1')->nullable();
            $table->float('mayoreo_2')->nullable();
            $table->float('mayoreo_3')->nullable();
            $table->float('mayoreo_4')->nullable();
            $table->integer('codigo_sat')->nullable();
            $table->integer('proveedor')->nullable();
            $table->integer('categoria')->nullable();
            $table->integer('linea')->nullable();
            $table->integer('marca')->nullable();
            $table->integer('existencia');
            $table->integer('local');
            $table->text('ubicacion_producto')->nullable();
            $table->text('fotos')->nullable();
            $table->text('palabra_clave')->nullable();
            $table->text('url')->nullable();
            $table->bigInteger('codigo_barra');
            $table->date('caducidad')->nullable();
            $table->text('descripcion_catalogo')->nullable();
            $table->timestamps();

/*
-id_articulo
-clave_articulo
-descripcion_articulo
-unidad
-precio de compra
-precio a la venta X % = ganancia
-mayoreo 1 X % = ganancia
-mayoreo 2 X % = ganancia
-mayoreo 3 X % = ganancia
-mayoreo 4 X % = ganancia
-codigo_sat
-proveedor 
-categoria
-linea
-marca
-existencia_tienda
-existencia_almacen
-existencia_sucursal
-ubicacion_producto SELECT(anden, vitrina, aparador, pasillo)
-fotos, 1,2,3,4,5
-palabra clave
-codigo de barra
*/


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('productos');
    }
}
