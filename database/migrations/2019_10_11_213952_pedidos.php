<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Pedidos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedidos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_cliente');
            $table->dateTime('fecha_pedido');
            $table->dateTime('fecha_enviado')->nullable();
            $table->dateTime('fecha_recibido')->nullable();
            $table->text('gps')->default("no disponible");
            $table->integer("cp")->nullable();
            $table->string("localidad")->nullable();
            $table->string("colonia")->nullable();
            $table->string("calle")->nullable();
            $table->integer("numero_exterior")->default(0);
            $table->integer("numero_interior")->default(0);
            $table->text("descripcion_lugar_envio")->default("no especificado");
            $table->text('ubicacion_actual')->default("en almacen");
            $table->decimal('importe', 12, 2)->default(0.00);
            $table->decimal('costo_envio', 12, 2)->default(0.00);
            $table->decimal('subtotal', 12, 2)->default(0.00);
            $table->decimal('descuento', 12, 2)->default(0.00);
            $table->decimal('total', 12, 2)->default(0.00);
            $table->string('status')->default("pendiente");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pedidos');
    }
}
