<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Ventas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {//Schema::connection('comments')->create(  ///ejemplo para utilizar otra BD
        Schema::create('ventas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('usuario');
            $table->integer('id_turno');
            $table->integer("caja")->nullable();
            $table->string('efectivo');
            $table->decimal('total_venta', 10, 2);
            $table->text('cantidad_pro');
            $table->text('id_productos');
            $table->text('precio_vendido');
            $table->timestamps();
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ventas');
    }
}
