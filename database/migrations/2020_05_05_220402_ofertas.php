<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Ofertas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ofertas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_producto')->unique();
            $table->string("unidad");
            $table->text('descripcion_producto');
            $table->float('precio_venta');
            $table->float('precio_oferta');
            $table->date('fecha_valido');
            $table->string('foto')->default("no-images.jpg");
            $table->text('url')->nullable();
            $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ofertas');
    }
}
