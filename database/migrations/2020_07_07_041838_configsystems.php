<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Configsystems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('configsystems', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre_negocio');
            $table->text('descripcion');
            $table->string('ubicacion')->nullable();
            $table->string('logotipo');
            $table->string('nombre_responsable');
            $table->string('apellidos');
            $table->string('telefono');
            $table->text('email');
            $table->text('precios_producto');
            $table->timestamps();
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('configsystems');
    }
}
