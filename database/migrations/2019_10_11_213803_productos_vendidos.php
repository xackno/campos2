<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProductosVendidos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prod_vendidos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->decimal('cantidad',12,2);
            $table->bigInteger('producto');
            $table->decimal('total',15,2);
            $table->bigInteger('id_venta');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prod_vendidos');
    }
}
