<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\configsystems;
use App\Models\locales;
use App\Models\rutas;
use App\Models\lineas;
use App\Models\marcas;
use App\Models\provedores;
use App\Models\categorias;
use Illuminate\Support\Facades\Hash;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
      // User::create([
      //       'name' => 'administrador',
      //       'user' => 'admin',
      //       'email' => 'xackno1995@gmail.com',
      //       'password' => Hash::make('admin123'),
      //       'type' => 'administrador',
      //       'local'=>0,
      //       'foto'=>"no-image.png"
      //   ]);

      configsystems::create([
            'nombre_negocio' => 'default',
            'descripcion' => 'para pruebas',
            'ubicacion' => 'ninguno',
            'logotipo' => 'new-logo.png',
            'nombre_responsable' => 'xackno',
            'apellidos'=>'Lorenzo Cruz',
            'telefono'=>'+529531768022',
            'email'=>'xackno1995@gmail.com',
            'precios_producto'=>",precio_compra/PSA,precio_venta/PV,mayoreo_1/PV,mayoreo_2/PV,mayoreo_3/PV,mayoreo_4/PV"
        ]);
      locales::create([
            'nombre' => 'general',
            'descripcion' => 'sin descripcion',
            'ubicacion'=>'no especificado'
        ]);
      rutas::create([
            'nombre_ruta' => 'niguno',
            'longitud_ruta' =>00,
            'tiempo_recorrido_total' =>'00:00:00',
            'origen' =>"0.000",
            'destino'=>"0.0000",
            'id_secciones'=>"%1",
            'nombre_secciones'=>"ninguno",
            "tiempo_secciones"=>"$00:00:00",
            "latitud_seccion"=>"%0.00,0.00"

        ]);
      lineas::create([
            'nombre' => 'general',
            'descripcion' => 'sin descripcion',
        ]);

        marcas::create([
            'nombre' => 'general',
            'descripcion' => 'sin descripcion',
        ]);

        provedores::create([
            'nombres' => 'general',
            'telefono' => '0000000000',
            'direccion' =>'sucursal',
        ]);

        categorias::create([
            'nombre' => 'general',
            'descripcion' => 'sin descripcion',
        ]);
    }
}
